-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 11-05-2020 a las 13:47:03
-- Versión del servidor: 5.7.29-0ubuntu0.18.04.1
-- Versión de PHP: 7.1.29-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `intranet_tecno_sms`
--
CREATE DATABASE IF NOT EXISTS `intranet_tecno_sms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `intranet_tecno_sms`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_usuarios`
--

CREATE TABLE `c_usuarios` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(55) DEFAULT NULL,
  `correo` varchar(55) DEFAULT NULL,
  `rol` varchar(55) DEFAULT NULL,
  `contrasena` varchar(50) NOT NULL,
  `telefono` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `c_usuarios`
--

INSERT INTO `c_usuarios` (`id`, `nombre`, `apellido`, `correo`, `rol`, `contrasena`, `telefono`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', 'Administrador', 'd033e22ae348aeb5660fc2140aec35850c4da997', '111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `import`
--

CREATE TABLE `import` (
  `id` int(11) NOT NULL COMMENT 'Primary Key',
  `first_name` varchar(100) NOT NULL COMMENT 'First Name',
  `last_name` varchar(100) NOT NULL COMMENT 'Last Name',
  `email` varchar(255) NOT NULL COMMENT 'Email Address',
  `contact_no` varchar(50) NOT NULL COMMENT 'Contact No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='datatable demo table';

--
-- Volcado de datos para la tabla `import`
--

INSERT INTO `import` (`id`, `first_name`, `last_name`, `email`, `contact_no`) VALUES
(1, 'Maria Figueroa', 'Desarrollo', '92258-98501', 'Desarrollo y analisis de sistemas'),
(2, 'Carlos Dulanto', 'Administracion', '92258-98502', 'Area administrativa'),
(3, 'Emerson Leoncio', 'Programacion', '92258-98503', 'Desarrollo de sistemas'),
(4, 'Martin Castellino', 'Back End', '92258-98504', 'Experto en panel de control'),
(5, 'Maria Figueroa', 'Desarrollo', '92258-98501', 'Desarrollo y analisis de sistemas'),
(6, 'Carlos Dulanto', 'Administracion', '92258-98502', 'Area administrativa'),
(7, 'Emerson Leoncio', 'Programacion', '92258-98503', 'Desarrollo de sistemas'),
(8, 'Martin Castellino', 'Back End', '92258-98504', 'Experto en panel de control'),
(9, 'Maria Figueroa', 'Desarrollo', '92258-98501', 'Desarrollo y analisis de sistemas'),
(10, 'Carlos Dulanto', 'Administracion', '92258-98502', 'Area administrativa'),
(11, 'Emerson Leoncio', 'Programacion', '92258-98503', 'Desarrollo de sistemas'),
(12, 'Martin Castellino', 'Back End', '92258-98504', 'Experto en panel de control'),
(13, 'Maria Figueroa', 'Desarrollo', '92258-98501', 'Desarrollo y analisis de sistemas'),
(14, 'Carlos Dulanto', 'Administracion', '92258-98502', 'Area administrativa'),
(15, 'Emerson Leoncio', 'Programacion', '92258-98503', 'Desarrollo de sistemas'),
(16, 'Martin Castellino', 'Back End', '92258-98504', 'Experto en panel de control'),
(17, 'Maria Figueroa', 'Desarrollo', '92258-98501', 'Desarrollo y analisis de sistemas'),
(18, 'Carlos Dulanto', 'Administracion', '92258-98502', 'Area administrativa'),
(19, 'Emerson Leoncio', 'Programacion', '92258-98503', 'Desarrollo de sistemas'),
(20, 'Martin Castellino', 'Back End', '92258-98504', 'Experto en panel de control');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ts_campana`
--

CREATE TABLE `ts_campana` (
  `idcampana` int(100) NOT NULL,
  `nombre_campana` varchar(100) DEFAULT NULL,
  `grupo_contactos` varchar(100) DEFAULT NULL,
  `sms_camapana` varchar(255) DEFAULT NULL,
  `ejecucion_camapana` int(100) DEFAULT NULL,
  `sms_enviados` varchar(100) DEFAULT NULL,
  `estatus_camapana` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ts_campana`
--

INSERT INTO `ts_campana` (`idcampana`, `nombre_campana`, `grupo_contactos`, `sms_camapana`, `ejecucion_camapana`, `sms_enviados`, `estatus_camapana`) VALUES
(1, 'campana1', 'grupo_1', 'okok', 1597952700, '2', 'ejecutada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ts_contacto`
--

CREATE TABLE `ts_contacto` (
  `idcontacto` int(100) NOT NULL,
  `nombre_contacto` varchar(100) DEFAULT NULL,
  `telefono_contacto` varchar(100) DEFAULT NULL,
  `rif_contacto` varchar(100) DEFAULT NULL,
  `grupo_contacto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ts_contacto`
--

INSERT INTO `ts_contacto` (`idcontacto`, `nombre_contacto`, `telefono_contacto`, `rif_contacto`, `grupo_contacto`) VALUES
(1, 'Maria Figueroa', '4129902187', '92258-98501', 'grupo_1'),
(2, 'Carlos Dulanto', '4261871878', '92258-98502', 'grupo_1'),
(3, 'Emerson Leoncio', 'Programacion', '92258-98503', 'grupo2'),
(4, 'Martin Castellino', 'Back End', '92258-98504', 'grupo2'),
(5, 'Maria Figueroa', 'Desarrollo', '92258-98501', 'grupo2'),
(6, 'Carlos Dulanto', 'Administracion', '92258-98502', 'grupo2'),
(7, 'Emerson Leoncio', 'Programacion', '92258-98503', 'grupo2'),
(8, 'Martin Castellino', 'Back End', '92258-98504', 'grupo2'),
(9, 'Maria Figueroa', 'Desarrollo', '92258-98501', 'grupo2'),
(10, 'Carlos Dulanto', 'Administracion', '92258-98502', 'grupo2'),
(11, 'Emerson Leoncio', 'Programacion', '92258-98503', 'grupo2'),
(12, 'Martin Castellino', 'Back End', '92258-98504', 'grupo2'),
(26, 'jose sanchez', '4261871878', 'j-123456780-1', 'fospuca'),
(27, 'jose sanchez', '4261871878', 'j-123456780-1', 'fospuca'),
(28, 'ysrrael sanchez', '4129902187', 'j-123456780-1', 'fospuca'),
(29, 'jose sanchez', '4261871878', 'j-123456780-1', 'fospuca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ts_grupo`
--

CREATE TABLE `ts_grupo` (
  `idgrupo` int(100) NOT NULL,
  `nombre_grupo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ts_grupo`
--

INSERT INTO `ts_grupo` (`idgrupo`, `nombre_grupo`) VALUES
(1, 'grupo_1'),
(2, 'grupo2'),
(3, 'fospuca'),
(4, 'fospuca'),
(5, 'fospuca'),
(6, 'fospuca'),
(7, 'fospuca'),
(8, 'fospuca'),
(9, 'fospuca'),
(10, 'fospuca'),
(11, 'fospuca'),
(12, 'fospuca'),
(13, 'fospuca'),
(14, 'fospuca'),
(15, 'fospuca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ts_sms`
--

CREATE TABLE `ts_sms` (
  `idsms` int(100) NOT NULL,
  `mensaje` varchar(200) DEFAULT NULL,
  `celular` varchar(100) DEFAULT NULL,
  `nombre_campana` varchar(100) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ts_sms`
--

INSERT INTO `ts_sms` (`idsms`, `mensaje`, `celular`, `nombre_campana`, `fecha`) VALUES
(1, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586415747', '2020-04-09 04:02:27'),
(2, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586415747', '2020-04-09 04:02:27'),
(3, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586415799', '2020-04-09 04:03:19'),
(4, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586415799', '2020-04-09 04:03:19'),
(5, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586416261', '2020-04-09 04:11:01'),
(6, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586416261', '2020-04-09 04:11:01'),
(7, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586416442', '2020-04-09 04:14:02'),
(8, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586416442', '2020-04-09 04:14:02'),
(9, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586416484', '2020-04-09 04:14:44'),
(10, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586416484', '2020-04-09 04:14:44'),
(11, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586416741', '2020-04-09 04:19:01'),
(12, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586416741', '2020-04-09 04:19:01'),
(13, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586417004', '2020-04-09 04:23:24'),
(14, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586417004', '2020-04-09 04:23:24'),
(15, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586417126', '2020-04-09 04:25:26'),
(16, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586417126', '2020-04-09 04:25:26'),
(17, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586417135', '2020-04-09 04:25:35'),
(18, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586417135', '2020-04-09 04:25:35'),
(19, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586417199', '2020-04-09 04:26:39'),
(20, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586417199', '2020-04-09 04:26:39'),
(21, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586417276', '2020-04-09 04:27:56'),
(22, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586417276', '2020-04-09 04:27:56'),
(23, 'esto es un mensaje de prueba app masiva', '4129902187', 'sms_direc_1586417337', '2020-04-09 04:28:57'),
(24, 'esto es un mensaje de prueba app masiva', '4261871878', 'sms_direc_1586417337', '2020-04-09 04:28:57');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `c_usuarios`
--
ALTER TABLE `c_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `import`
--
ALTER TABLE `import`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ts_campana`
--
ALTER TABLE `ts_campana`
  ADD PRIMARY KEY (`idcampana`);

--
-- Indices de la tabla `ts_contacto`
--
ALTER TABLE `ts_contacto`
  ADD PRIMARY KEY (`idcontacto`);

--
-- Indices de la tabla `ts_grupo`
--
ALTER TABLE `ts_grupo`
  ADD PRIMARY KEY (`idgrupo`);

--
-- Indices de la tabla `ts_sms`
--
ALTER TABLE `ts_sms`
  ADD PRIMARY KEY (`idsms`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `c_usuarios`
--
ALTER TABLE `c_usuarios`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `import`
--
ALTER TABLE `import`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `ts_campana`
--
ALTER TABLE `ts_campana`
  MODIFY `idcampana` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ts_contacto`
--
ALTER TABLE `ts_contacto`
  MODIFY `idcontacto` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `ts_grupo`
--
ALTER TABLE `ts_grupo`
  MODIFY `idgrupo` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `ts_sms`
--
ALTER TABLE `ts_sms`
  MODIFY `idsms` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
