<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept');
class Welcome extends CI_Controller {
	
	public function __construct(){
    	parent::__construct();
        //always check if session userdata value "logged_in" is not true
         $this->load->helper('tools_helper');
         $this->load->model('censo_model');

        
    }


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$view_data["menu"] = false;

		$this->load->view('Head', $view_data);
		$this->load->view('login');
		$this->load->view('Footer', $view_data);
	}

	public function contribuyente(){
			$rif = $_GET["rif"];
			$data = $this->censo_model->search_contribuyente_by_rif($rif);
			echo json_encode($data);

	}


}
