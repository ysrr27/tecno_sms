<?php 

	$this->load->model('censo_model');
	switch($this->uri->segment(3)){
		default:
		case "search": 
		case "home": 

			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor") {
					redirect('censo/censo/editar');
				}
			}else{censo/
				redirect('/');
			}	

		    $response = new StdClass();	

			$response->rows = $this->censo_model->get_Censos();
			$view_data["data"] = $response;

			$view_data["menu"] = true;
			$this->load->view('Head', $view_data);
			$this->load->view('censo/home', $view_data);
			$this->load->view('Footer', $view_data);
		
		break;

		case "editar":
		case "preview":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
			}else{
				redirect('/');
			}

			$objCenso = new StdClass();
			$objEmpadronador = new StdClass();


			$view_data["menu"] = true;

			$id = $this->uri->segment(4);
			if( $id == "" or $id == "0"){
				$id = 0;
			}
			 $objCenso = $this->censo_model->get_byIdCenso( $id );
			 $objEmpadronador = $this->censo_model->get_empadronador();

			 $view_data["empadronador"] = $objEmpadronador;	
			 $view_data["data"] = $objCenso;

			 if( $this->uri->segment(3) == "editar"){

				$this->load->view('Head', $view_data);
				$this->load->view('censo/editar',$view_data);
				$this->load->view('Footer', $view_data);

			 }else if( $this->uri->segment(3) == "preview"){

			 	$this->load->view('Head', $view_data);
				$this->load->view('censo/preview',$view_data);
				$this->load->view('Footer', $view_data);

			 }	

		break;
		case "aprobar":

  	  	if($this->session->userdata('logueado')){


  	  		$idcenso = $_GET["id"];
  	  		$idcenso2 = (int) $idcenso;
  	  		$ArrRegisterCenso = array();
  	  		$ArrRegisterCenso['estatus']  = "Corregida";
  	  		$ArrRegisterCenso['supervisor']  = $this->session->userdata('id');
		
		$data = $this->censo_model->aprobar($idcenso2,$ArrRegisterCenso);

		echo json_encode($data);

  	  	}else{
  	  		redirect('/');
  	  	}


		break;
		case "update":

		/*$postdata = file_get_contents("php://input");
  	  	$request = json_decode($postdata);*/

  	  	if($this->session->userdata('logueado')){


		$postdata = file_get_contents("php://input");
  	  	$request = json_encode($_POST);
  	  	$requestpost = json_decode($request);


  	  	$objContribuyente = new StdClass();
  	  	$objActividadEconomica = new StdClass();
  	  	$objInmuebles = new StdClass();
  	  	$objExpendioLicores = new StdClass();
  	  	$objPublicidad = new StdClass();
  	  	$objDetallesEstablecimiento = new StdClass();
  	  	$objApuestas = new StdClass();
  	  	$objServElectrico = new StdClass();
  	  	$objDatosPersonales = new StdClass();
  	  	$ArrRegisterCenso = array();

  		$rol_user = $this->session->userdata('rol');


		/* Obj Identificacion del Contribuyente */
			$idcontribuyente							= @$requestpost->idcontribuyente;
			$objContribuyente->idcontribuyente			= (int) $idcontribuyente;
			$objContribuyente->rif			 			= @$requestpost->rif;
			$objContribuyente->razon_social   			= @$requestpost->razon_social;
			$objContribuyente->denominacion_comercial   = @$requestpost->denominacion_comercial;
			$objContribuyente->avenida_calle  			= @$requestpost->avenida_calle;
			$objContribuyente->edif_quinta  			= @$requestpost->edif_quinta;
			$objContribuyente->piso  					= @$requestpost->piso;
			$objContribuyente->punto_referencia  		= @$requestpost->punto_referencia;
			$objContribuyente->cod_telefono_1  			= @$requestpost->cod_telefono_1;
			$objContribuyente->telefono_1  				= @$requestpost->telefono_1;
			$objContribuyente->e_mail  					= @$requestpost->e_mail;
			$objContribuyente->local  					= @$requestpost->local;
			$objContribuyente->medidas  				= @$requestpost->medidas;
			$objContribuyente->uso  					= @$requestpost->uso;
			$objContribuyente->rrss  					= @$requestpost->rrss;
			$objContribuyente->tipo_negocio  			= @$requestpost->tipo_negocio;

		/*Obj Actividad Economica */
			$idactividad  									=  @$requestpost->idactividad;
			$objActividadEconomica->idactividad  			=  (int) $idactividad;
			$objActividadEconomica->licencia_AE 	 		=  @$requestpost->licencia_AE;
			$objActividadEconomica->anio_licencia  	    	=  @$requestpost->anio_licencia;
			$objActividadEconomica->exhibida  				=  @$requestpost->exhibida;
			$objActividadEconomica->declaracion_AE  		=  @$requestpost->declaracion_AE;
			$objActividadEconomica->mes_declaracion_AE  	=  @$requestpost->mes_declaracion_AE;
			$objActividadEconomica->ano_declaracion_AE  	=  @$requestpost->ano_declaracion_AE;
			$objActividadEconomica->num_cuenta_renta  		=  @$requestpost->num_cuenta_renta;
			$objActividadEconomica->lic_a_e  				=  @$requestpost->lic_a_e;
			$objActividadEconomica->exhibe_AE  				=  @$requestpost->exhibe_AE;
			$objActividadEconomica->activ_eco_autorizadas  	=  @$requestpost->activ_eco_autorizadas;
			$objActividadEconomica->activ_eco_observadas  	=  @$requestpost->activ_eco_observadas;

		/*Obj  Inmuebles urbanos */
			$idinmueble							= @$requestpost->idinmueble;
			$objInmuebles->idinmueble			= (int) $idinmueble;
			$objInmuebles->numero_catastral		= @$requestpost->numero_catastral;
			$objInmuebles->solvencia_inmueble	= @$requestpost->solvencia_inmueble;
			$objInmuebles->ano_solvencia		= @$requestpost->ano_solvencia;
			$objInmuebles->inmueble				= @$requestpost->inmueble;
			$objInmuebles->datos				= @$requestpost->datos;
			$objInmuebles->vehiculos			= @$requestpost->vehiculos;
			$objInmuebles->placa				= @$requestpost->placa;
			$objInmuebles->propietario			= @$requestpost->propietario;
			$objInmuebles->serial				= @$requestpost->serial;

		/*Obj Licencia para el expendio de licores */
			$idexpendio									= @$requestpost->idexpendio;
			$objExpendioLicores->idexpendio				= (int) $idexpendio;
			$objExpendioLicores->licencia_licores		= @$requestpost->licencia_licores;
			$objExpendioLicores->ano_licencia_lic		= @$requestpost->ano_licencia_lic;
			$objExpendioLicores->tipo_expendio			= @$requestpost->tipo_expendio;
			$objExpendioLicores->tipo_expendio			= @$requestpost->tipo_expendio;
			$objExpendioLicores->licencia_exhibida		= @$requestpost->licencia_exhibida;
			$objExpendioLicores->nro_licencia			= @$requestpost->nro_licencia;

		/*Obj Apuestas lícitas*/
			$idapuestas							= @$requestpost->idapuestas;
			$objApuestas->idapuestas			= (int) $idapuestas;
			$objApuestas->apuestas_licitas		= @$requestpost->apuestas_licitas;
			$objApuestas->cantidad_maquinas		= @$requestpost->cantidad_maquinas;
			$objApuestas->maquina_traganiquel	= @$requestpost->maquina_traganiquel;

		/*Obj Publicidad comercial*/
			$idpublicidad								= @$requestpost->idpublicidad;
			$objPublicidad->idpublicidad				= (int) $idpublicidad;
			$objPublicidad->aviso_publicitario			= @$requestpost->aviso_publicitario;
			$objPublicidad->cantidad_avisos				= @$requestpost->cantidad_avisos;
			$objPublicidad->tipo_aviso					= @$requestpost->tipo_aviso;
			$objPublicidad->cantidad_caras				= @$requestpost->cantidad_caras;

			/*Obj Servicio eléctric*/ 
			$idservicio											= @$requestpost->idservicio;
			$objServElectrico->idservicio						= (int) $idservicio;
			$objServElectrico->contrato_electricidad			= @$requestpost->contrato_electricidad;

			/* Obj Datos personales*/
			$iddatos											= @$requestpost->iddatos;
			$objDatosPersonales->iddatos						= (int) $iddatos;
			
			$objDatosPersonales->contribuyente					= @$requestpost->contribuyente;
			$objDatosPersonales->cedula_contribuyente			= @$requestpost->cedula_contribuyente;
			$objDatosPersonales->cod_telf_fijo_contribuyente	= @$requestpost->cod_telf_fijo_contribuyente;
			$objDatosPersonales->telf_fijo_contribuyente		= @$requestpost->telf_fijo_contribuyente;
			$objDatosPersonales->cod_telf_movil_contribuyente	= @$requestpost->cod_telf_movil_contribuyente;
			$objDatosPersonales->telf_movil_contribuyente		= @$requestpost->telf_movil_contribuyente;
			$objDatosPersonales->email_contribuyente			= @$requestpost->email_contribuyente;
			


			$objDatosPersonales->reprensentante_legal		= @$requestpost->reprensentante_legal;
			$objDatosPersonales->cedula_representante		= @$requestpost->cedula_representante;
			$objDatosPersonales->cod_telf_fijo_representante= @$requestpost->cod_telf_fijo_representante;
			$objDatosPersonales->telf_fijo_representante	= @$requestpost->telf_fijo_representante;
			$objDatosPersonales->cod_telf_movil_representante= @$requestpost->cod_telf_movil_representante;
			$objDatosPersonales->telf_movil_representante	= @$requestpost->telf_movil_representante;
			$objDatosPersonales->email_reprensentante_legal	= @$requestpost->email_reprensentante_legal	;

			$objDatosPersonales->atendidopor					= @$requestpost->atendidopor;
			$objDatosPersonales->cedula_atendidopor			= @$requestpost->cedula_atendidopor;
			$objDatosPersonales->cod_telf_fijo_atendidopor= @$requestpost->cod_telf_fijo_atendidopor;
			$objDatosPersonales->telf_fijo_atendidopor		= @$requestpost->telf_fijo_atendidopor;
			$objDatosPersonales->cod_telf_movil_atendidopor		= @$requestpost->cod_telf_movil_atendidopor;
			$objDatosPersonales->telf_movil_atendidopor		= @$requestpost->telf_movil_atendidopor;
			$objDatosPersonales->email_atendidopor			= @$requestpost->email_atendidopor;

			$idcenso = @$requestpost->idcenso;

			 $idcenso2 = (int) $idcenso;

		 
		/*array censo*/
		$ArrRegisterCenso['observaciones']  = @$requestpost->observaciones;
		$ArrRegisterCenso['empadronador']  = @$requestpost->empadronador;
		$ArrRegisterCenso['verificacion_telefonica']  = @$requestpost->verificacion_telefonica;
		
		if( !isset($idcenso) or $idcenso =="0" or $idcenso==""){   
			$ArrRegisterCenso['estatus']  = "Sin Corrección";
			$ArrRegisterCenso['usuario']  = $this->session->userdata('id');
		}else{
			$ArrRegisterCenso['estatus']  = "Corregida";
			$ArrRegisterCenso['supervisor']  = $this->session->userdata('id');
		}

		
		$ArrRegisterCenso['fecha']  = date("d/m/Y");
		$ArrRegisterCenso['tr']  = @$requestpost->tr;

		$data = $this->censo_model->addCenso($idcenso2,$objContribuyente,$objActividadEconomica,$objInmuebles,$objExpendioLicores,$objPublicidad,$objDetallesEstablecimiento,$objApuestas,$objServElectrico,$objDatosPersonales,$ArrRegisterCenso);

		echo json_encode($data);



  	  	}else{
  	  		redirect('/');
  	  	}

		break;	
		case 'delete':
				//echo "deleted:".$_GET["idsuscripcion"];
		$idcenso = $_GET["id"];
		$idcenso2 = (int) $idcenso;

		$data = $this->censo_model->deleteUser($idcenso2);

		echo json_encode($data);

		break;	
	}


 ?>