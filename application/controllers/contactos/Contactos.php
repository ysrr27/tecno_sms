<?php 

	$this->load->model('contacto_model');
    $this->load->helper(array('url','html','form'));
	switch($this->uri->segment(3)){
		default:
		case "search": 
		case "home": 

			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');

				$response = new StdClass();	

				$response->rows = $this->contacto_model->groups();
				$view_data["data"] = $response;

				$view_data["menu"] = true;
				$this->load->view('Head', $view_data);
				$this->load->view('contactos/home', $view_data);
				$this->load->view('Footer', $view_data);

			}else{
				redirect('/');
			}	

		
		break;
		case "preview":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');

				$objgrupo = new StdClass();
				$view_data["menu"] = true;

				$id = $this->uri->segment(4);
				if( $id == "" or $id == "0"){
					$id = 0;
				}

				$objgrupo = $this->contacto_model->get_byidgrupo( $id );
				$view_data["data"] = $objgrupo;
				$this->load->view('Head', $view_data);
				$this->load->view('contactos/preview',$view_data);
				$this->load->view('Footer', $view_data);

			}else{
				redirect('/');
			}
		break;

		case "editar":
		case "grupos":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
			}else{
				redirect('/');
			}

			$objCenso = new StdClass();
			$objEmpadronador = new StdClass();


			$view_data["menu"] = true;

			$id = $this->uri->segment(4);
			if( $id == "" or $id == "0"){
				$id = 0;
			}
			 $objCenso = $this->censo_model->get_byIdCenso( $id );
			 $objEmpadronador = $this->censo_model->get_empadronador();

			 $view_data["empadronador"] = $objEmpadronador;	
			 $view_data["data"] = $objCenso;

			 if( $this->uri->segment(3) == "editar"){

				$this->load->view('Head', $view_data);
				$this->load->view('contactos/editar',$view_data);
				$this->load->view('Footer', $view_data);

			 }else if( $this->uri->segment(3) == "preview"){

			 	$this->load->view('Head', $view_data);
				$this->load->view('contactos/preview',$view_data);
				$this->load->view('Footer', $view_data);

			 }	

		break;
		case 'delete':
		$idgrupo = $_GET["id"];
		$idgrupo2 = (int) $idgrupo;

		$data = $this->contacto_model->deletecontactos($idgrupo2);

		echo json_encode($data);

		break;	
		case "lista": 

			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor") {
					redirect('contactos/contactos/editar');
				}
			}else{
				redirect('/');
			}	

		    $response = new StdClass();	

			$response->rows = $this->censo_model->get_Censos();
			$view_data["data"] = $response;

			$view_data["menu"] = true;
			$this->load->view('Head', $view_data);
			$this->load->view('contactos/contacto', $view_data);
			$this->load->view('Footer', $view_data);
		
		break;


		case "crear":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
			}else{
				redirect('/');
			}




			$view_data["menu"] = true;

			$id = $this->uri->segment(4);
			if( $id == "" or $id == "0"){
				$id = 0;
			}


			$this->load->view('Head', $view_data);
			$this->load->view('contactos/crear',$view_data);
			$this->load->view('Footer', $view_data);

		break;
		case "importcontacto":

		if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
			

		 if ($this->input->post('submit')) {
             	
             	$grupo = new StdClass();	
 
                $path = 'uploads/';
                require_once APPPATH . "/third_party/PHPExcel.php";
                $config['upload_path'] = $path;
                $config['allowed_types'] = 'xlsx|xls|csv';
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);     

                if (!$this->upload->do_upload('uploadFile')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                }

                if(empty($error)){
                  if (!empty($data['upload_data']['file_name'])) {
                    $import_xls_file = $data['upload_data']['file_name'];
                } else {
                    $import_xls_file = 0;
                }


                $inputFileName = $path . $import_xls_file;
                 
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                    $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $flag = true;
                    $i=0;
                    foreach ($allDataInSheet as $value) {
                    /*  if($flag){
                        $flag =false;
                        continue;
                      }*/

                      $cant_number = strlen($value['B']);
                      $cod = intval(substr($value['B'], 0, -7));                      

	                     if($cant_number == 10 && $cod == 412|| $cod == 424|| $cod == 414|| $cod == 416|| $cod == 426){
		                      $inserdata[$i]['nombre_contacto'] = $value['A'];
		                      $inserdata[$i]['telefono_contacto'] = $value['B'];
		                      $inserdata[$i]['rif_contacto'] = $value['C'];
		                      $inserdata[$i]['grupo_contacto'] = $value['D'];
		                      $grupo->nombre_grupo = $value['D'];
		                      $i++;
		                  }else{
		                  		$url = get_site_url("contactos/contactos/editar");

							  die("<b>'".$value['B']."'</b> Verifique la columna 'B' numeros telefonicos incorrectos<br><a href='".$url."'>Volver</a>"); 
						  } 
	                  } 

       				$grupo_contacto =  $this->contacto_model->addgrupo($grupo);
                    $result = $this->contacto_model->addcontacto($inserdata);   
                    if($result){
                      redirect('/contactos/contactos/preview/'.$grupo_contacto);
                    }else{
                      echo "ERROR !";
                    }             
      
              } catch (Exception $e) {
                   die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                            . '": ' .$e->getMessage());
                }
              }else{
                  echo $error['error'];
                }
                 
                 
        }
        }else{
				redirect('/');
			}

		break;
		case 'getcontactos':
		$idcontacto = (int)$_GET["id"];

		$data = $this->contacto_model->get_contacto($idcontacto);
		echo json_encode($data);

		break;	
		case 'update_contact':	  	
		if($this->session->userdata('logueado')){
			$postdata = file_get_contents("php://input");
			$request = json_encode($_POST);
			$requestpost = json_decode($request);

			$objContacto = new StdClass();

			$objContacto->idcontacto 		= (int)@$requestpost->idcontacto;
			$objContacto->nombre_contacto 	= @$requestpost->nombre_contacto;
			$objContacto->telefono_contacto = @$requestpost->telefono_contacto;
			$objContacto->rif_contacto 		= @$requestpost->rif_contacto;
			$objContacto->grupo_contacto 	= @$requestpost->grupo_contacto;

			$data = $this->contacto_model->update_contact($objContacto);
			echo json_encode($data);

		}
		break;
		case 'delete_contact':
			$idcontacto = (int)$_GET["id"];

			$data = $this->contacto_model->delete_contact($idcontacto);

			echo json_encode($data);
		break;		
	}


 ?>