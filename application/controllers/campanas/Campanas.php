<?php 

	$this->load->model('campanas_model');
	$this->load->helper(array('url','html','form'));
	$this->load->helper('date');

	switch($this->uri->segment(3)){
		default:
		case "search": 
		case "home": 

			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');

				$response = new StdClass();	

				$response->rows = $this->campanas_model->getcampanas();
				$view_data["data"] = $response;

				$view_data["menu"] = true;
				$this->load->view('Head', $view_data);
				$this->load->view('campanas/home', $view_data);
				$this->load->view('Footer', $view_data);

			}else{
				redirect('/');
			}	

		
		break;
		case "preview":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');


				$objcampana = new StdClass();
				$grupos = new StdClass();
				$view_data["menu"] = true;

				$id = $this->uri->segment(4);
				if( $id == "" or $id == "0"){
					$id = 0;
				}

				$objcampana = $this->campanas_model->get_bycampanapreviw($id);

				$view_data["data"] = $objcampana;
				$view_data["grupos"] = $grupos;

				$this->load->view('Head', $view_data);
				$this->load->view('campanas/preview',$view_data);
				$this->load->view('Footer', $view_data);

			}else{
				redirect('/');
			}
		break;

		case "editar":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');


				$objcampana = new StdClass();
				$grupos = new StdClass();
				$view_data["menu"] = true;

				$id = $this->uri->segment(4);
				if( $id == "" or $id == "0"){
					$id = 0;
				}

				$objcampana = $this->campanas_model->get_bycampana( $id );
				$grupos = $this->campanas_model->getgrupos();

				$view_data["data"] = $objcampana;
				$view_data["grupos"] = $grupos;

				$this->load->view('Head', $view_data);
				$this->load->view('campanas/editar',$view_data);
				$this->load->view('Footer', $view_data);

			}else{
				redirect('/');
			}


		break;
		case 'delete':
				//echo "deleted:".$_GET["idsuscripcion"];
		$idcampana = $_GET["id"];
		$idcampana2 = (int) $idcampana;

		$data = $this->campanas_model->deletecampana($idcampana2);

		echo json_encode($data);

		break;	
		case "lista": 

			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor") {
					redirect('contactos/contactos/editar');
				}
			}else{
				redirect('/');
			}	

		    $response = new StdClass();	
			$response->rows = $this->censo_model->get_Censos();
			$view_data["data"] = $response;
			$view_data["menu"] = true;
			$this->load->view('Head', $view_data);
			$this->load->view('contactos/contacto', $view_data);
			$this->load->view('Footer', $view_data);	
		break;

		case "update_campana":      	
             	$objcampana = new StdClass();	
             	$objcampana->idcampana = $this->input->post('idcampana', TRUE);
             	$objcampana->nombre_campana = $this->input->post('nombre_campana', TRUE);
             	$objcampana->grupo_contactos = $this->input->post('grupo_contactos', TRUE);
             	$objcampana->sms_camapana = $this->input->post('sms_camapana', TRUE);

             	$date_ejecution = strtotime($this->input->post('ejecucion_camapana', TRUE));
             	if (!empty($date_ejecution)) {
             		$objcampana->ejecucion_camapana = date('d/m/Y H:i', $date_ejecution);
             	}else{
             		$objcampana->ejecucion_camapana = date('d/m/Y H:i',time());
             	}
             
             	$objcampana->sms_enviados = 0;
             	$objcampana->estatus_camapana = "pendiente";
             	$result = $this->campanas_model->addcampana($objcampana);   
             	if($result){
             		redirect('/campanas/campanas/preview/'.$result);
             	}else{
             		echo "ERROR !";
             	}             
		break;
	}


 ?>