<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

     /**Ysrrael Sanchez
    *  08.04.20202
    *  Envio de sms    
    */
     public function __construct(){
        parent::__construct();
        $this->load->helper('tools_helper');
        $this->load->helper(array('url','html','form'));
        $this->load->helper('date');
        $this->load->model('censo_model');
        $this->load->model('sms_model');

    }
    

    public function index(){   

        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['rol'] = $this->session->userdata('rol');

            $response = new StdClass(); 
            $view_data["data"] = $response;
            $view_data["menu"] = true;
            $this->load->view('Head', $view_data);
            $this->load->view('sms/home', $view_data);
            $this->load->view('Footer', $view_data);

        }else{
            redirect('/');
        }        
    }

    public function importFile(){

        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['rol'] = $this->session->userdata('rol');

            if ($this->input->post('submit')) {
                $objsmsdirect = new StdClass();

                $path = 'uploads/';
                require_once APPPATH . "/third_party/PHPExcel.php";
                $config['upload_path'] = $path;
                $config['allowed_types'] = 'xlsx|xls|csv';
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);     

                if (!$this->upload->do_upload('uploadFile')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                }

                if(empty($error)){
                    if (!empty($data['upload_data']['file_name'])) {
                        $import_xls_file = $data['upload_data']['file_name'];
                    } else {
                        $import_xls_file = 0;
                    }


                    $inputFileName = $path . $import_xls_file;
                    $campana_name = "sms_direc_".local_to_gmt(time());

                    $date1 = strtotime('-4 hour',time());
                    $date = strtotime('+1 minute',$date1);
                    $timesend = date('d/m/Y H:i',$date);

                    try {
                        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($inputFileName);
                        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                        $flag = true;
                        $i=0;
                        foreach ($allDataInSheet as $value) {
                            $inserdata[$i]['celular'] = $value['A'];
                            $inserdata[$i]['mensaje'] = $value['B'];
                            $inserdata[$i]['nombre_campana'] = $campana_name;
                            $inserdata[$i]['fecha'] = $timesend;
                            $i++;
                        }

                    $objsmsdirect->nombre_campana = $campana_name;
                    $objsmsdirect->sms_cantidad = $i;

                        $result = $this->sms_model->insert($inserdata);
                        $view_data["idsmsdirect"] = $this->sms_model->sms_direct($objsmsdirect);                        
                        $view_data["nombre_campana"] = $campana_name;
                        $view_data["sms_cantidad"] = $i;
                        
                        if($result){
                            $view_data["menu"] = true;
                            $this->load->view('Head', $view_data);
                            $this->load->view('sms/sms_ejecute', $view_data);
                            $this->load->view('Footer', $view_data);

                        }else{
                            echo "ERROR !";
                        }             

                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                            . '": ' .$e->getMessage());
                    }
                }else{
                    echo $error['error'];
                }


            }
        }
    }

    public function send_sms($idsmsdirect,$campana_name){



        /*$postdata = file_get_contents("php://input");
        $request = json_encode($_POST);
        $requestpost = json_decode($request);*/
        $objsmsdirect = new StdClass();
        $objsmsdirect->idsmsdirect = $idsmsdirect;
        $nombre_campana  = $campana_name;


        $records = $this->sms_model->get_contactos_direct($nombre_campana);
        /*var_dump($records);
        exit();*/

        $i = 0;
        foreach ($records as $record) {
            $i++;
            $objsmsdirect->sms_enviados = $i;
            $celular = $record->celular;
            $mensaje = $record->mensaje;
            $result = $this->sms_model->update_sms_direct($objsmsdirect);


        if (!empty($celular) && !empty($mensaje)) {

            $number = "58".$celular;
            $message = $mensaje;

            ini_set('soap.wsdl_cache_enabled', 0); 
            ini_set('soap.wsdl_cache_ttl', 900); 
            ini_set('default_socket_timeout', 15); 

            $params = array( 
                'passport' => 'VXTECNO', 
                'password' => '4SB2e5pcUc', 
                'number' => $number, 
                'text' => $message, 
            ); 

            $wsdl = 'http://api.tedexis.com:8086/m4.in.wsint/services/M4WSIntSR?wsdl'; 
            $options = array( 
                'uri'=>'http://schemas.xmlsoap.org/soap/envelope/', 
                'style'=>SOAP_RPC, 
                'use'=>SOAP_ENCODED, 
                'soap_version'=>SOAP_1_1, 
                'cache_wsdl'=>WSDL_CACHE_NONE, 
                'connection_timeout'=>15,  
                'trace'=>true,
                'encoding'=>'UTF-8', 
                'exceptions'=>true, 
            ); 
            try { 
               $soap = new SoapClient($wsdl, $options); 
               $data = $soap->sendSMS($params);
             //$result = $this->sms_model->update_sms_direct($objsmsdirect);
            } 
            catch(Exception $e) { 
                die($e->getMessage()); 
            } 
            $data2['success'] = true;
            $data2['message'] = 'Success!';
            echo true;
        }else{
            echo false;
        }
        }




    }

    public function mensajes_enviados(){ 
        $idsmsdirect = (int)$_GET["idsmsdirect"];
        $data = $this->sms_model->get_sms_enviados($idsmsdirect);
        echo json_encode($data);
    }
    

}
