<?php 
	$this->load->model('reporte_model');

	switch($this->uri->segment(3)){
		default:
		case "search": 
		case "home":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor") {
					redirect('censo/censo/editar');
				}
			}else{
				redirect('/');
			} 
			$view_data["menu"] = true;
			$this->load->view('Head', $view_data);
			$this->load->view('reportes/home');
			$this->load->view('Footer', $view_data);

		break;

		case "export":

		if($this->session->userdata('logueado')){


			$postdata = file_get_contents("php://input");
			$request = json_encode($_POST);
			$requestpost = json_decode($request);
			
			$result = $this->reporte_model->get_export();

			$html = "<table border='1'>";
			$html.="<tr>";
			$html.="<th> Nro. Control </th>";
			$html.="<th> Razon Social </th>";
			$html.="<th> Denominacion Comercial </th>";
			$html.="<th> Rif </th>";
			$html.="<th> Local </th>";
			$html.="<th> Medidas </th>";
			$html.="<th> Uso </th>";
			$html.="<th> Avenida / Calle </th>";
			$html.="<th> Punto de referencia </th>";
			$html.="<th> Edif. / Quinta / Casa </th>";
			$html.="<th> Piso </th>";
			$html.="<th> Telefono </th>";
			$html.="<th> Redes Sociales </th>";
			$html.="<th> Num.Cta.contrato Electricidad </th>";
			$html.="<th> Correo Electronico </th>";
			$html.="<th> Tipo de Negocio </th>";
			$html.="<th> Número de cuenta renta </th>";
			$html.="<th> Licencia de Actividades Economicas </th>";
			$html.="<th> Exibida </th>";
			$html.="<th> año </th>";
			$html.="<th> Liciencia de A.E. Nro </th>";
			$html.="<th> ¿Presentó Declaracion de A.E? </th>";
			$html.="<th> Mes </th>";
			$html.="<th> Año </th>";
			$html.="<th> ¿Exhibe Declaracion de A.E? </th>";
			$html.="<th> ¿Presento Solvencia de Inmueble? </th>";
			$html.="<th> Año </th>";
			$html.="<th> Inmueble </th>";
			$html.="<th> Datos </th>";
			$html.="<th> Numero Catastro </th>";
			$html.="<th> ¿Posee vehículos? </th>";
			$html.="<th> placa </th>";
			$html.="<th> Propietario </th>";
			$html.="<th> Serial </th>";
			$html.="<th> ¿Presento Licencia de Licores? </th>";
			$html.="<th> Año </th>";
			$html.="<th> Tipo de Expendio </th>";
			$html.="<th> Nro. Licencia </th>";
			$html.="<th> Exhibida </th>";
			$html.="<th> Permiso de Apuestas Licitas </th>";
			$html.="<th> Maquina traganiqueles </th>";
			$html.="<th> Cantidad de Máquinas </th>";
			$html.="<th> ¿Permiso de publicidad? </th>";
			$html.="<th> Cantidad de Avisos </th>";
			$html.="<th> Tipo de Avisos </th>";
			$html.="<th> Cantidad de Caras </th>";
			$html.="<th> Actividades Económicas Autorizadas </th>";
			$html.="<th> Actividades Económicas Observadas </th>";
			$html.="<th> Observaciones </th>";
			$html.="<th> Contribuyente </th>";
			$html.="<th> Cedula Nro. </th>";
			$html.="<th> Telefono Fijo </th>";
			$html.="<th> Telefono Movil </th>";
			$html.="<th> E-mail </th>";
			$html.="<th> Representante Legal </th>";
			$html.="<th> Cedula Nro. </th>";
			$html.="<th> Telefono Fijo </th>";
            $html.="<th> Telefono Movil </th>";
			$html.="<th> E-mail </th>";
			$html.="<th> Atendido por </th>";
			$html.="<th> Cedula Nro. </th>";
			$html.="<th> Telefono Fijo </th>";
			$html.="<th> Telefono Movil </th>";
			$html.="<th> E-mail </th>";
			$html.="<th> Empadronador </th>";
			$html.="<th> TR </th>";
			$html.="</tr>";
			$x = 0; 
			$html .= "<tr>";


			foreach($result as $row){
				foreach($row as $clave => $valor) {
					$html .= "<td>".$valor."</td>";

				}
				$html .= "</tr>";
				$x = $x + 1 ;
			}
			$html.= "</table>";

		//	$html = "<table border='1'><tr><td>ok</td></tr></table>";
			$filena= "Reporte_censo_".date("d/m/Y");

			header("Content-Type: application/xls");    
			header("Content-Disposition: attachment; filename=".$filena.".xls");  
			header("Pragma: no-cache"); 
			header("Expires: 0");

			echo $html;

			$view_data["cabeceras"] = $requestpost;
			$view_data["data"] = $result;
			$view_data["menu"] = true;
			$this->load->view('Head', $view_data);
			$this->load->view('reportes/export');
			$this->load->view('Footer', $view_data);
			exit();

		//	echo json_encode($data);

		}else{
			redirect('/');
		}

	
		break;

		
	}


 ?>