<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 /**CHRISTIAN MANRIQUE GARICA
	*  02.11.2015
	*  DECLARE THE CONSTRUCTOR TO INICIALIZE THE SESSION LIBARY AND VALIDATE IF THE USER IS LOGGED 		
	*/
	public function __construct(){
    	parent::__construct();
        //always check if session userdata value "logged_in" is not true

                 $this->load->helper('tools_helper');
                 $this->load->model('campanas_model');


    }

    
	 
	public function index()
	{	

		if($this->session->userdata('logueado')){
			$view_data = array();
			$view_data['nombre'] = $this->session->userdata('nombre');
			$view_data['rol'] = $this->session->userdata('rol');
			$view_data["menu"] = true;

			$response = new StdClass();	


			$response->rows = $this->campanas_model->getcampanasejecutadas();
			$response->rowspendientes = $this->campanas_model->getcampanaspendientes();
			$count = $this->campanas_model->get_count_campanas();
			$count_grupos = $this->campanas_model->get_count_grupos();
			



			$view_data["data"] = $response;
			$view_data["count"] = $count;
			$view_data["count_grupos"] = $count_grupos;

			$this->load->view('Head', $view_data);
			$this->load->view('Dashboard',$view_data);
			$this->load->view('Footer', $view_data);
		}else{
			redirect('/');
		}

	}
	

}
