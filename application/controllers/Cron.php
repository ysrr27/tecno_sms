<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {
	
	public function __construct(){
    	parent::__construct();
    	$this->load->library('session');
    	$this->load->model('sms_model');
    }
    

    public function sendsms(){
    	if($this->input->is_cli_request()){
            $date = strtotime('-4 hour',time());
            $timesend = date('d/m/Y H:i', $date);



    		$result = $this->sms_model->get_contactos_sms($timesend);
	     
            $cant_sms = count($result);
    		$count = 1;
    	    $objcampana = new StdClass();

    		if (!empty($result) && $cant_sms>0) {
    			foreach ($result as $key => $value) {
    				$sms_enviados = ($count+$key);
    				echo "Enviando ".$sms_enviados." de ".$cant_sms."\n";
            error_log(print_r("sendsms".$timesend." -> Enviando ".$sms_enviados." de ".$cant_sms."\n", TRUE), 3, '/var/tmp/errors.log');
    				$number = "58".intval($value->celular);
    				$message = $value->sms_camapana;

    				if (!empty($number) && !empty($message)) {
    					ini_set('soap.wsdl_cache_enabled', 0); 
    					ini_set('soap.wsdl_cache_ttl', 900); 
    					ini_set('default_socket_timeout', 15); 

    					$params = array( 
    						'passport' => 'VXTECNO', 
    						'password' => '4SB2e5pcUc', 
    						'number' => $number, 
    						'text' => $message, 
    					); 

    					$wsdl = 'http://api.tedexis.com:8086/m4.in.wsint/services/M4WSIntSR?wsdl'; 
    					$options = array( 
    						'uri'=>'http://schemas.xmlsoap.org/soap/envelope/', 
    						'style'=>SOAP_RPC, 
    						'use'=>SOAP_ENCODED, 
    						'soap_version'=>SOAP_1_1, 
    						'cache_wsdl'=>WSDL_CACHE_NONE, 
    						'connection_timeout'=>15,  
    						'trace'=>true,
    						'encoding'=>'UTF-8', 
    						'exceptions'=>true, 
    					); 

    					try { 
    						$soap = new SoapClient($wsdl, $options); 
    						$data = $soap->sendSMS($params);

                            error_log(print_r("sendsms -> Enviando ".$sms_enviados." de ".$cant_sms."\n", TRUE), 3, '/var/tmp/errors.log');

    						if ($sms_enviados == $cant_sms) {
    							$objcampana->idcampana = intval($value->idcampana);
    							$objcampana->estatus_camapana = "ejecutada";
    							$objcampana->sms_enviados = $sms_enviados;
    							$result_estatus = $this->sms_model->update_status($objcampana);
    						}


    					} 
    					catch(Exception $e) { 
    						die($e->getMessage()); 
    					} 
    					$data2['success'] = true;
    					$data2['message'] = 'Success!';
    					echo "Enviado con exito \n";

    				}else{
    					echo "error faltan datos";
    					continue;
    				}
    			}

    		}

    	}
    }

    public function send_sms_direct(){
        if($this->input->is_cli_request()){
            $date = strtotime('-4 hour',time());
            $timesend = date('d/m/Y H:i', $date);

            error_log(print_r($timesend.'smsdirect', TRUE), 3, '/var/tmp/errors.log');

            $records = $this->sms_model->get_contactos_direct($timesend);


            $cant_sms = count($records);
            $count = 1;
            $objcampana = new StdClass();
            $objsmsdirect = new StdClass();
            if (!empty($records) && $cant_sms>0) {
                foreach ($records as $key => $value) {
                    $sms_enviados = ($count+$key);
                    echo "Enviando ".$sms_enviados." de ".$cant_sms."\n";
                    $number = "58".intval($value->celular);
                    $message = $value->mensaje;
                    $objsmsdirect->nombre_campana = $value->nombre_campana;
                    $objsmsdirect->sms_enviados = $sms_enviados;
                    if (!empty($number) && !empty($message)) {
                        ini_set('soap.wsdl_cache_enabled', 0); 
                        ini_set('soap.wsdl_cache_ttl', 900); 
                        ini_set('default_socket_timeout', 15); 

                        $params = array( 
                            'passport' => 'VXTECNO', 
                            'password' => '4SB2e5pcUc', 
                            'number' => $number, 
                            'text' => $message, 
                        ); 

                        $wsdl = 'http://api.tedexis.com:8086/m4.in.wsint/services/M4WSIntSR?wsdl'; 
                        $options = array( 
                            'uri'=>'http://schemas.xmlsoap.org/soap/envelope/', 
                            'style'=>SOAP_RPC, 
                            'use'=>SOAP_ENCODED, 
                            'soap_version'=>SOAP_1_1, 
                            'cache_wsdl'=>WSDL_CACHE_NONE, 
                            'connection_timeout'=>15,  
                            'trace'=>true,
                            'encoding'=>'UTF-8', 
                            'exceptions'=>true, 
                        ); 

                        try { 
                            $soap = new SoapClient($wsdl, $options); 
                            $data = $soap->sendSMS($params);

                            error_log(print_r("smsdirect -> Enviando ".$sms_enviados." de ".$cant_sms."\n", TRUE), 3, '/var/tmp/errors.log');


                            $this->sms_model->update_sms_direct($objsmsdirect);


                        } 
                        catch(Exception $e) { 
                           // die($e->getMessage()); 
                        } 
                        $data2['success'] = true;
                        $data2['message'] = 'Success!';
                        echo "Enviado con exito \n";

                    }else{
                        echo "error faltan datos";
                        continue;
                    }
                }

            }

        }
    }

}