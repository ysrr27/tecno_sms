<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
    class Contacto_model extends CI_Model {
 
        public function __construct()
        {
            $this->load->database();
        }
        
        public function addcontacto($data) {
  
            $res = $this->db->insert_batch('ts_contacto',$data);
            if($res){
                return TRUE;
            }else{
                return FALSE;
            }
      
        }


        public function addgrupo($data) {
            $this->db->insert('ts_grupo', $data);             
            $idgrupo = $this->db->insert_id();

            return $idgrupo;
        }


        public function get_byidgrupo($id){           
          $this->db->select('*');
          $this->db->from('ts_grupo');
          $this->db->where('idgrupo', $id);
          $this->db->order_by('idgrupo',"desc");
          $this->db->limit(1);

          $consulta = $this->db->get();
          $grupo = $consulta->row();

                //$response = new StdClass();         

                $sql = " SELECT *
                FROM ts_contacto
                WHERE grupo_contacto = '".$grupo->nombre_grupo."'";
                $query = $this->db->query( $sql );
                $result = $query->result();
    
                return $result;


        }

        public function groups(){

           $sql = "SELECT tsg.idgrupo, tsg.nombre_grupo, count(tsc.idcontacto) as count
                    FROM ts_grupo tsg
                    INNER JOIN ts_contacto tsc ON (tsg.nombre_grupo = tsc.grupo_contacto)
                    group by tsg.nombre_grupo";
           $query = $this->db->query( $sql );
           $result = $query->result();

           return $result;

        }

        public function deletecontactos($idgrupo){

        $this->db->select('nombre_grupo');
        $this->db->from('ts_grupo');
        $this->db->where('idgrupo', $idgrupo);
        $this->db->limit(1);
        $consulta = $this->db->get();
        $group_name = $consulta->row();

        
        $this->db->where('grupo_contacto',$group_name->nombre_grupo);
        $this->db->delete('ts_contacto');
        $result = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 

        $this->db->where('idgrupo',$idgrupo);
        $this->db->delete('ts_grupo');
        $result = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 


          return $result;


        }

        public function get_contacto($idcontacto) {

          $this->db->select('*');
          $this->db->from('ts_contacto');
          $this->db->where('idcontacto', $idcontacto);
          $this->db->limit(1);
          $consulta = $this->db->get();
          $result = $consulta->row();
        
        return  $result;
      }

      public function update_contact($objContacto) {
        $this->db->update('ts_contacto' , $objContacto, array('idcontacto' =>  $objContacto->idcontacto));
        $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        return $resultado;  

     }

      public function delete_contact($idcontacto) {

            $this->db->where('idcontacto',$idcontacto);
            $this->db->delete('ts_contacto');
            return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 

    }



  

    }
?>