<?php
class Reporte_model extends CI_Model { 
   public function __construct() {
      parent::__construct();
   }



   public function get_export(){
        $sql = "SELECT CONCAT('000',cen.idcenso) as idcenso,idc.razon_social,idc.denominacion_comercial,idc.rif,idc.local,idc.medidas,idc.uso,idc.avenida_calle,idc.punto_referencia,idc.edif_quinta,idc.piso,CONCAT(idc.cod_telefono_1, '-', idc.telefono_1) AS telf,idc.rrss,ser.contrato_electricidad,idc.e_mail,idc.tipo_negocio,act.num_cuenta_renta,act.lic_a_e,act.exhibida,act.anio_licencia,act.licencia_AE,act.declaracion_AE,act.mes_declaracion_AE,act.ano_declaracion_AE,act.exhibe_AE,
        inm.solvencia_inmueble,inm.ano_solvencia,inm.inmueble,inm.datos,inm.numero_catastral,inm.vehiculos,inm.placa,inm.propietario,inm.serial,exp.licencia_licores,exp.ano_licencia_lic,exp.tipo_expendio,exp.nro_licencia,exp.licencia_exhibida,apu.apuestas_licitas,apu.maquina_traganiquel,apu.cantidad_maquinas,pub.aviso_publicitario,pub.cantidad_avisos,pub.tipo_aviso,pub.cantidad_caras,act.activ_eco_autorizadas,act.activ_eco_observadas,cen.observaciones,dat.contribuyente,dat.cedula_contribuyente,CONCAT(dat.cod_telf_fijo_contribuyente, '-',dat.telf_fijo_contribuyente) AS telefcontribuyente,CONCAT(dat.cod_telf_movil_contribuyente, '-',dat.telf_movil_contribuyente) AS telefmovilcontribuyente,dat.email_contribuyente,
        dat.reprensentante_legal,dat.cedula_representante,CONCAT(dat.cod_telf_fijo_representante, '-',dat.telf_fijo_representante) AS telefrepresentante,CONCAT(dat.cod_telf_movil_representante, '-',dat.telf_movil_representante) AS telefmovilrepresentante,dat.email_reprensentante_legal,dat.atendidopor,dat.cedula_atendidopor,CONCAT(dat.cod_telf_fijo_atendidopor, '-',dat.telf_fijo_atendidopor) AS telefatendidopor,CONCAT(dat.cod_telf_movil_atendidopor, '-',dat.telf_movil_atendidopor) AS telefmovilatendidopor,dat.email_atendidopor,cen.empadronador,cen.tr
        FROM c_censo cen
        INNER JOIN c_contribuyente idc ON (cen.idcontribuyente = idc.idcontribuyente) 
        INNER JOIN c_actividad_economica AS act ON (cen.idactividad = act.idactividad)
        INNER JOIN c_inmuebles AS inm ON (cen.idinmueble = inm.idinmueble)
        INNER JOIN c_expendio_licores AS exp ON(cen.idexpendio=exp.idexpendio) 
        INNER JOIN c_publicidad AS pub ON(cen.idpublicidad = pub.idpublicidad) 
        INNER JOIN c_apuestas AS apu ON(cen.idapuestas = apu.idapuestas) 
        INNER JOIN c_serv_electrico AS ser ON(cen.idservicio = ser.idservicio) 
        INNER JOIN c_datos_personales AS dat ON(cen.iddatos = dat.iddatos) 
        INNER JOIN c_usuarios AS usr ON(cen.usuario = usr.id)";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }
  

}













