<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
    class Sms_model extends CI_Model {
 
        public function __construct(){
            $this->load->database();
        }
        

        public function insert($data) {

            $res = $this->db->insert_batch('ts_sms',$data);
            if($res){
                return TRUE;
            }else{
                return FALSE;
            }

        }
     

       public function sms_direct($objsmsdirect) {

         $this->db->insert('ts_sms_direct', $objsmsdirect);             
         $idsmsdirect = $this->db->insert_id();
         return $idsmsdirect;

     }
      


        public function groups(){

           $sql = "SELECT tsg.idgrupo, tsg.nombre_grupo, count(tsc.idcontacto) as count
                    FROM ts_grupo tsg
                    INNER JOIN ts_contacto tsc ON (tsg.nombre_grupo = tsc.grupo_contacto)
                    group by tsg.nombre_grupo";
           $query = $this->db->query( $sql );
           $result = $query->result();

           return $result;

        }


      public function getgrupos(){
        $sql = "SELECT * FROM ts_grupo ORDER BY idgrupo DESC";

        $query = $this->db->query( $sql );
        $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
    }

        public function getcampanas(){
            $sql = "SELECT tsc.idcampana,tsc.nombre_campana,tsc.grupo_contactos,tsc.sms_camapana,DATE_FORMAT(FROM_UNIXTIME(tsc.ejecucion_camapana), '%e/%m/%Y %H:%i') AS ejecucion_camapana,tsc.sms_enviados,tsc.estatus_camapana FROM ts_campana tsc ORDER BY idcampana DESC";



            $query = $this->db->query( $sql );
            $result = $query->result();

            if ($result)
                return $result;
            else
                return false;
        }

        public function get_contactos_sms($timesend){

            $sql = "SELECT tsca.idcampana,tsca.nombre_campana,tsca.grupo_contactos,tsca.sms_camapana,tsca.ejecucion_camapana AS ejecucion_camapana,tsc.telefono_contacto AS celular
            FROM ts_campana tsca
            INNER JOIN ts_contacto tsc ON (tsca.grupo_contactos = tsc.grupo_contacto)
            WHERE ejecucion_camapana ='".$timesend."' AND estatus_camapana = 'pendiente'";
            $query = $this->db->query( $sql );
            $result = $query->result();

            return $result;

        }

        public function get_contactos_direct($fecha){

            $sql = "SELECT *
            FROM ts_sms 
            WHERE fecha ='".$fecha."'";
            $query = $this->db->query( $sql );
            $result = $query->result();

            return $result;

        }


        public function update_status($objcampana){

           $this->db->update('ts_campana' , $objcampana, array('idcampana' =>  $objcampana->idcampana));
           $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        
        }

        public function update_sms_direct($objsmsdirect){
            var_dump($objsmsdirect);
            $this->db->set('sms_enviados', $objsmsdirect->sms_enviados); 
            $this->db->where('nombre_campana', $objsmsdirect->nombre_campana);
            $this->db->update('ts_sms_direct');
            $resultado = $this->db->insert_id();
            return $resultado;
        }

        public function get_sms_enviados($idsmsdirect){


          $this->db->select('sms_enviados,sms_cantidad');
          $this->db->from('ts_sms_direct');
          $this->db->where('idsmsdirect', $idsmsdirect);
          $consulta = $this->db->get();
          $resultado = $consulta->row();

          return $resultado;
      }


    }
?>