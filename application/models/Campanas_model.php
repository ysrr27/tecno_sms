<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
    class Campanas_model extends CI_Model {
 
        public function __construct(){
            $this->load->database();
        }
      
   public function addcampana($objcampana){

      if( !isset($objcampana->idcampana) or $objcampana->idcampana =="0" or $objcampana->idcampana==""){

        $this->db->insert('ts_campana', $objcampana );             
        $resultado = $this->db->insert_id();
        return $resultado;

        }else{
            $this->db->update('ts_campana' , $objcampana, array('idcampana' =>  $objcampana->idcampana));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if($resultado){
                return $objcampana->idcampana;  
            } 
        }
       

   }

        public function get_bycampanapreviw($id){
           
           $sql = "SELECT *
                    FROM ts_campana tsca
                    INNER JOIN ts_contacto tsc ON (tsca.grupo_contactos = tsc.grupo_contacto)
                    WHERE idcampana =".$id;
           $query = $this->db->query( $sql );
           $result = $query->result();

           return $result;

        }

        public function get_bycampana($id){
           
           $sql = "SELECT tsca.idcampana,tsca.nombre_campana,tsca.grupo_contactos,tsca.sms_camapana,DATE_FORMAT(FROM_UNIXTIME(tsca.ejecucion_camapana), '%Y-%m-%eT%H:%i') AS ejecucion_camapana
                    FROM ts_campana tsca
                    INNER JOIN ts_contacto tsc ON (tsca.grupo_contactos = tsc.grupo_contacto)
                    WHERE idcampana =".$id;
           $query = $this->db->query( $sql );
           $result = $query->result();

           return $result;

        }


        public function groups(){

           $sql = "SELECT tsg.idgrupo, tsg.nombre_grupo, count(tsc.idcontacto) as count
                    FROM ts_grupo tsg
                    INNER JOIN ts_contacto tsc ON (tsg.nombre_grupo = tsc.grupo_contacto)
                    group by tsg.nombre_grupo";
           $query = $this->db->query( $sql );
           $result = $query->result();

           return $result;

        }


      public function getgrupos(){
        $sql = "SELECT * FROM ts_grupo ORDER BY idgrupo DESC";

        $query = $this->db->query( $sql );
        $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
    }

    public function getcampanas(){
        $sql = "SELECT tsc.idcampana,tsc.nombre_campana,tsc.grupo_contactos,tsc.sms_camapana,tsc.ejecucion_camapana AS ejecucion_camapana,tsc.sms_enviados,tsc.estatus_camapana FROM ts_campana tsc ORDER BY idcampana DESC";

        $query = $this->db->query( $sql );
        $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
    }

    public function getcampanasejecutadas(){
        $sql = "SELECT tsc.idcampana,tsc.nombre_campana,tsc.grupo_contactos,tsc.sms_camapana,tsc.ejecucion_camapana AS ejecucion_camapana,tsc.sms_enviados,tsc.estatus_camapana 
                FROM ts_campana tsc
                WHERE  tsc.estatus_camapana = 'ejecutada'
                ORDER BY idcampana DESC LIMIT 10";



        $query = $this->db->query( $sql );
        $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
    }

     public function getcampanaspendientes(){
        $sql = "SELECT tsc.idcampana,tsc.nombre_campana,tsc.grupo_contactos,tsc.sms_camapana,tsc.ejecucion_camapana AS ejecucion_camapana,tsc.sms_enviados,tsc.estatus_camapana 
                FROM ts_campana tsc
                WHERE  tsc.estatus_camapana = 'pendiente'
                ORDER BY idcampana DESC LIMIT 10";



        $query = $this->db->query( $sql );
        $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
    }

    public function get_count_campanas(){
        $sql="SELECT count(idcampana) AS num
              FROM ts_campana";

               $query = $this->db->query( $sql );
                    $result = $query->result();
                      if ($result)
                          return $result;
                      else
              return false;

    }

    public function get_count_grupos(){
        $sql="SELECT count(idgrupo) AS num
              FROM ts_grupo";

               $query = $this->db->query( $sql );
                    $result = $query->result();
                      if ($result)
                          return $result;
                      else
              return false;

    }

    public function deletecampana($idcampana){

        $this->db->where('idcampana',$idcampana);
        $this->db->delete('ts_campana');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }








    }
?>