<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                  <a>Home</a>
                  Reportes
                </div>
                  <div class="x_content">

                    <div class="animated flipInY ">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-download"></i><small>DATOS PARA EXPORTAR</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- start accordion -->
                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                    <form action="<?php echo get_site_url("reportes/reportes/export")?>" id="export" name="export" role="form" data-toggle="validator" method="post" accept-charset="utf-8">

                    <!-- end of accordion -->

                    <button type="submit" class="btn btn-primary  sw-btn-next"> <i class="fa fa-file-excel-o"></i> Exportar en Excel</button>
                  </form>     

                  </div>
                </div>
              </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

