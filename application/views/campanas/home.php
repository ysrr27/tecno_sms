
 <style type="text/css">
     .display{
          display: none;
     }
</style>

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                        <a>Home</a>
                        <a>Campañas</a>
                        Lista de campañas
                   </div>
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div>
                  <div id="loading-wrapper">
                    <div id="loading-text">Cargando...</div>
                    <div id="loading-content"></div>
                </div>
                  <div class="x_content">

                  <table id="campanas" class="row-border display" style="width:100%">
                      <thead>
                          <tr>
                              <th>Acción</th>
                              <th>Nombre de la campaña</th>
                              <th>Grupo de contactos</th>
                              <th>Fecha de ejecución</th>
                              <th>SMS</th>
                              <th>SMS Enviados</th>
                              <th>Estatus</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php 
                      

                        if(is_array($data->rows )){
                          foreach(  $data->rows as $key =>$row){
                                    ?>
                                  <tr>
                                      <td>
                                        <div class="btn-group  btn-group-sm">
                                          <a href="<?php echo get_site_url('campanas/campanas/preview/'.$row->idcampana)?>" class="btn btn-default sw-btn-next" type="button" title=”VER”><i class="fa fa-eye"></i></a>
                                           <a href="<?php echo get_site_url('campanas/campanas/editar/'.$row->idcampana)?>" class="btn btn-default sw-btn-next" type="button" title=”EDITAR”><i class="fa fa-edit"></i></a>
                                          <button class="btn btn-xs btn-default btn-action" name="Activar" data-toggle="tooltip" title=”Borrar” data-placement="right" id="<?php echo  $row->idcampana?>"><i class="fa fa-times"></i></button> 
                                        </div>
                                      </td>
                                      <td><?php echo  $row->nombre_campana ?></td>
                                      <td><?php echo  $row->grupo_contactos ?></td>
                                      <td><?php echo  $row->ejecucion_camapana ?></td>
                                      <td><?php echo  $row->sms_camapana ?></td>
                                      <td><?php echo  $row->sms_enviados ?></td>
                                      <td><?php echo  $row->estatus_camapana ?></td>
                                
                                  </tr>
                                  <?php

                                }
                              }                          
                        ?>
                      </tbody>
                      <tfoot>
                          <tr>
                             <td colspan="7"></td>
                          </tr>
                      </tfoot>
                  </table>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

