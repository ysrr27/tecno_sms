<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                    <a>Home</a>
                    Grupo de contactos
                </div>
                <div class="x_panel">
                    <div class="x_title">
                        <h2><small><i class="fa fa-bullhorn"></i> Campaña <?php echo $data[0]->nombre_campana ?> ( <?php echo $data[0]->estatus_camapana ?> ) </small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="rif">Mensaje:</label>
                                <span class="view-text"><?php echo $data[0]->sms_camapana?></span>
                            </div>

                            <div class="col-md-12">
                                <label for="denominacion_comercial">Fecha de Ejecución:</label>
                                <span class="view-text"><?php  echo $data[0]->ejecucion_camapana;
                               //  $new_date_format = date('d/m/Y H:i', $data[0]->ejecucion_camapana);echo $new_date_format ?></span> 
                            </div>  
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="rif">Grupo de contactos:</label>
                                <span class="view-text"><?php echo $data[0]->grupo_contacto?></span>
                            </div>
                        </div>
                        <div class="row">
                            <!-- NEW COL START -->
                            <article class="col-sm-12 col-md-12 col-lg-12">
                                <fieldset>
                                    <div class="row">
                                        <div class="x_title">
                                            <br>
                                            <label for="denominacion_comercial">Lista de contactos:</b></label>

                                            <div class="row">

                                                <table id="censo" class="row-border display" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre</th>
                                                            <th>Teléfono</th>
                                                            <th>Rif</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        if(is_array($data)){
                                                            foreach(  $data as $key =>$row){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo  $row->nombre_contacto ?></td>
                                                                    <td><?php echo  $row->telefono_contacto ?></td>
                                                                    <td><?php echo  $row->rif_contacto ?></td>
                                                                </tr>
                                                                <?php

                                                            }
                                                        }                          
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="3"></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </article>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

