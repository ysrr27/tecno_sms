
 <style type="text/css">
     .display{
          display: none;
     }
</style>

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                        <a>Home</a>
                        Contacto
                   </div>
                <div class="x_panel">
                
                  <div class="x_title pull-right ">
                    <a href="<?php echo get_site_url('contactos/contactos/crear')?> " class="btn btn-primary  sw-btn-next"> <i class="fa fa-plus"></i> Crear</a>
                    <div class="clearfix"></div>
                  </div>
                  <div id="loading-wrapper">
                    <div id="loading-text">Cargando...</div>
                    <div id="loading-content"></div>
                </div>
                  <div class="x_content">

                  <table id="censo" class="row-border display" style="width:100%">
                      <thead>
                          <tr>
                              <th>Teléfono</th>
                              <th>Nombre</th>
                              <th>Apellido</th>
                              <th>Correo</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php 
                      

                        if(is_array($data->rows )){
                          foreach(  $data->rows as $key =>$row){
                                    ?>
                                  <tr>
                                      <td>
                                        <div class="btn-group  btn-group-sm">
                                          <a href="<?php echo get_site_url('censo/censo/editar/'.$row->idcenso)?>" class="btn btn-default sw-btn-next" type="button" title=”EDITAR”><i class="fa fa-edit"></i></a>
                                          <button class="btn btn-xs btn-default btn-action" name="Activar" data-toggle="tooltip" title=”Borrar” data-placement="right" id="<?php echo  $row->idcenso?>"><i class="fa fa-times"></i></button> 
                                        </div>
                                      </td>
                                      <td>000<?php echo  $row->idcenso ?></td>
                                      <td><?php echo  $row->rif ?></td>
                                      <td><?php echo  $row->denominacion_comercial ?></td>
                                  </tr>
                                  <?php

                                }
                              }                          
                        ?>
                      </tbody>
                      <tfoot>
                          <tr>
                             <td colspan="4"></td>
                          </tr>
                      </tfoot>
                  </table>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

