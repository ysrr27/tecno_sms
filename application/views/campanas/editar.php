<!-- page content -->


<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="breadcrumb">
                    <a>Home</a>Campaña
                </div>
                <div class="x_panel">
                    <div class="x_content">

                        <div class="x_title">
                            <h3><small><i class="fa fa-th-large"></i> CAPAÑA</small></h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="panel-body">
                                    <div class="outer-container">
                                     <form class="form-horizontal form-label-left input_mask" action="<?php echo get_site_url("campanas/campanas/update_campana")?>" method="post">  
                                        <input type="hidden" id="idcampana" name="idcampana" value="<?php echo (!empty($data[0]->idcampana) && $data[0]->idcampana > '') ? $data[0]->idcampana : ''; ?>">

                                        <div class="col-md-6 col-sm-10 col-xs-12 form-group">
                                            <label>Nombre de la Campaña</label>

                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-bullhorn"></i>
                                                </span>
                                                <input type="text" class="form-control" name="nombre_campana" id="nombre_campana" placeholder="Nombre de la Campaña" value="<?php echo (!empty($data[0]->nombre_campana) && $data[0]->nombre_campana > '') ? $data[0]->nombre_campana : ''; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-10 col-xs-12 form-group">
                                            <label>Grupo de Contactos</label>
                                            <select name="grupo_contactos" id="grupo_contactos"  class="form-control">
                                                <?php echo (!empty($data[0]->grupo_contactos) && $data[0]->grupo_contactos > '') ? "<option value='".$data[0]->grupo_contactos."' selected>".$data[0]->grupo_contactos."</option>" : '<option value="" selected>Grupo de contactos</option>'; 
                                                foreach ($grupos as $grupo) {
                                                    echo "<option value='".$grupo->nombre_grupo."'>".$grupo->nombre_grupo."</option>";
                                                }?>
                                            </select>
                                        </div>
                                        <div class="col-md-12 col-sm-10 col-xs-12 form-group">
                                            <label for="sms_camapana">Mensaje</label>
                                            <textarea id="sms_camapana" required="required" class="form-control" name="sms_camapana"><?php echo (!empty($data[0]->sms_camapana) && $data[0]->sms_camapana > '') ? $data[0]->sms_camapana : ''; ?></textarea>
                                        </div>
                                        <div class="col-md-8 col-sm-10 col-xs-12 form-group">
                                            <div class="form-group">
                                                <label><i class="fa fa-cogs"></i> Ejecución</label> <br><br>
                                                <div class="col-10" style="overflow:hidden;">
                                                   <div id="datetimepicker12"></div>
                                                   <input type="hidden"  id="ejecucion_camapana" name="ejecucion_camapana" value="<?php echo (!empty($data[0]->ejecucion_camapana) && $data[0]->ejecucion_camapana > '') ? $data[0]->ejecucion_camapana : ''; ?>">
                                               </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-7 col-sm-9 col-xs-12 col-md-offset-5">
                                                <button type="submit" id="enviar" class="btn sw-btn-next" style="display: block !important;" name="enviar"><i class="fa fa-cloud-upload"></i> Enviar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->