 
    </div>
    </div>
  <!-- footer content -->



<?php if($menu){?>    
        <footer>
          <div class="pull-right">
            ©2020 Todos los derechos reservados. TecnoRed
          </div>
          <div class="clearfix"></div>
        </footer>
<?php } ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo get_assets_url();?>assets/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo get_assets_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo get_assets_url();?>assets/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo get_assets_url();?>assets/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo get_assets_url();?>assets/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo get_assets_url();?>assets/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="<?php echo get_assets_url();?>assets/Flot/jquery.flot.js"></script>
    <script src="<?php echo get_assets_url();?>assets/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo get_assets_url();?>assets/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo get_assets_url();?>assets/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo get_assets_url();?>assets/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo get_assets_url();?>assets/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo get_assets_url();?>assets/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo get_assets_url();?>assets/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo get_assets_url();?>assets/moment/min/moment.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- bootstrap-datetimepicker -->    
    <script src="<?php echo get_assets_url();?>assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="<?php echo get_assets_url();?>assets/js/custom.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/moment-with-locales.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/bootstrap-datetimepicker.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>

    <script type="text/javascript" src="<?php echo get_assets_url();?>assets/jQuery-Smart-Wizard/dist/js/jquery.smartWizard.js"></script>
     <script src="<?php echo get_assets_url();?>assets/datatables.net/js/jquery.dataTables.min.js"></script>

    <script src="<?php echo get_assets_url();?>assets/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/libs/1.11.4-jquery-ui.min.js"></script>

      <!-- jquery.inputmask -->
    <script src="<?php echo get_assets_url();?>assets/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>

        <script src="<?php echo get_assets_url();?>assets/alertify/lib/alertify.min.js"></script> 
        <script type="text/javascript">

<?php 
    $url = $this->uri->segment(2);
if($url == 'importFile'){
?>

            <?php if(!empty($idsmsdirect) or intval($idsmsdirect) > 0){?>    
                  
                            function get_sms_enviados() {
                                var idsmsdirect = <?php echo $idsmsdirect; ?>;
                                var id = $(".sms_send").attr('id');
                                var cantidad = <?php echo $sms_cantidad; ?>;
                                if (parseInt(id) < parseInt(cantidad)) {
                                    $.ajax({
                                        url: '<?php echo get_site_url('sms/mensajes_enviados')?>',
                                        type: 'get',
                                        data: { 'idsmsdirect' : idsmsdirect},
                                        success: function (data) {
                                            console.log(data);
                                            var obj = jQuery.parseJSON(data);
                                            $(".sms_send").empty();
                                            $(".sms_send").append(obj.sms_enviados);

                                            if (parseInt(obj.sms_enviados) < parseInt(cantidad)) {
                                                $('.sms_send').attr("id", obj.sms_enviados); 
                                            }

                                            if (parseInt(obj.sms_enviados) === parseInt(cantidad)) {
                                                $(".load-3").hide();
                                            }


                                        },error: function (err) {
                                            console.log(err);
                                        }
                                    });
                                    return false;
                                } 
                            }
             
                                setInterval(get_sms_enviados, 4000);
            <?php } ?>

            <?php } ?>





            setTimeout(function() {
                $(".child_menu").fadeOut();
                $("#loading-wrapper").fadeOut(1500);
            },100);

            $('#myDatepicker4').datetimepicker({
                ignoreReadonly: true,
                allowInputToggle: true
            });

            $(function () {
                $('#datetimepicker12').datetimepicker({
                    inline: true,
                    sideBySide: true
                });
                $('#datetimepicker12').on('dp.change', function(event) {
                    //console.log(moment(event.date).format('MM/DD/YYYY h:mm a'));
                    //console.log(event.date.format('MM/DD/YYYY h:mm a'));
                    $('#selected-date').text(event.date);
                    var formatted_date = event.date.format('MM/DD/YYYY h:mm a');
                    $('#ejecucion_camapana').val(formatted_date);
                    console.log(formatted_date);
                    });
            });

        function buscar() {
          var textoBusqueda = $(".search_tcontacto").val();
          if (textoBusqueda != "") {
            $.post('<?php echo get_site_url("censo/ajax_get_contribuyente")?>', {valorBusqueda: textoBusqueda}, function(mensaje) {

              if (mensaje ==="true") {
                $("#resultadoBusqueda").html("<div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true' onclick='myFunction()'>×</span></button> <strong>"+textoBusqueda+"</strong> este Rif ya se encuentra registrado </div>");

              }else{
               $("#resultadoBusqueda").html("");
             }





            }); 
          } else { 
            $("#resultadoBusqueda").html('');
          };
        }

        window.addEventListener("keypress", function(event){
          if (event.keyCode == 13){
            event.preventDefault();
          }
        }, false);

        function myFunction() {
         $(".search_tcontacto").val('');
        }
        $(document).ready(function() {

     $(".currency").inputmask('currency',{rightAlign: true  });




    $('.btn-aprobar').on( 'click', function () {
        var idcenso = jQuery(this).attr("id");
          alertify.confirm("¿Esta seguro que desea aprobar este registro?",
            function (e) {
                if (e) {
                    $.ajax({
                        url: '<?php echo get_site_url('censo/censo/aprobar/')?>',
                        type: 'get',
                        data: { 'id' : idcenso},
                        success: function (data) {
                            console.log(data);
                            var simple = '<?php echo get_site_url("censo/censo/preview")?>';
                            window.location.replace(simple+"/"+data);
                            alertify.success('Censo Aprobado');
                        },error: function (err) {
                            console.log(err);
                        }
                    });
                    return false;
                } else {
                    alertify.error("Usted ha cancelado la solicitud");

                }
            },
            function () {
                var error = alertify.error('Cancel');
            });
         
         } );


   $('#myForm').submit(function(e) {
        e.preventDefault();
        var form = $("#myForm");
        var url = form.attr('action');
        var simple = '<?php echo get_site_url("censo/censo/preview")?>';

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), 
            success: function(data)
            {
               $data = data;
               if ($data != "false") {
                    window.location.replace(simple+"/"+data);
               }
          }
     });
   });


  $( ".search_cnae" ).autocomplete({
    minLength: 2,
    source: '<?php echo get_site_url("censo/get_cnae")?>',
    select: function( event, ui ) {
    
      $(this ).val(ui.item.label);
      partner_id = $( this ).attr("data-partner-id");
      partner_id2 = $( this ).attr("data-partner-id2");
      $( "#"+partner_id ).val(ui.item.label);
      $( "#"+partner_id2 ).val(ui.item.label);

      return false;

    }
  }).focus(function () {
      $(this).val('').autocomplete("search");
   });


        isOPERA = (navigator.userAgent.indexOf('Opera') >= 0)? true : false;
        isIE    = (document.all && !isOPERA)? true : false;
        isDOM   = (document.getElementById && !isIE && !isOPERA)? true : false;


          $('#Otros').change(function() {
            if (this.value == 'OTROS') {
              $("#text_otro").show();
            }
            else{
             $("#text_otro").hide();
            }
          });


          $('input[type=radio]').change(function() {

            classname = "."+this.name;
            
            if (this.value == 'NO' || this.value == 'N/A') {
              $(classname).prop('disabled', true);
            }
            else{
             $(classname).prop('disabled', false);
            }
          });


        $('.formatoRif').keypress(function(e){
        if (this.value.length + 1 > 12) return false;
        return formatoRif(this,e);
        });

          function formatoRif(campo, e){

            var patron =/^[jgvpeJGVPE\d]$/;
            if (isDOM) codigoTecla = e.which;
            else if (isIE) codigoTecla = e.keyCode;
            if (codigoTecla == 13) return true;
            if (codigoTecla == 8) return true;
            if (codigoTecla == 0) return true;
            var tecla = String.fromCharCode(codigoTecla);
            if (tecla.search(patron)==-1) return false;

            switch (campo.value.length){
                case 0:
                if (tecla.search(/^[jgvpeJGVPE]$/)==-1)
                    return false;
                break;
                case 1:case 10:
                if (tecla.search(/^\d$/)!=-1)
                    campo.value=campo.value + "-" + tecla;
                return false;
                break;
                default:
                if (tecla.search(/^\d$/)==-1)
                    return false;
            }

            campo.value+=tecla.toUpperCase();
            return false;
        }


         $('.cedula').keypress(function(e){
        if (this.value.length + 1 > 10) return false;
        return cedula(this,e);
        });

          function cedula(campo, e){

            var patron =/^[veVE\d]$/;
            if (isDOM) codigoTecla = e.which;
            else if (isIE) codigoTecla = e.keyCode;
            if (codigoTecla == 13) return true;
            if (codigoTecla == 8) return true;
            if (codigoTecla == 0) return true;
            var tecla = String.fromCharCode(codigoTecla);
            if (tecla.search(patron)==-1) return false;

            switch (campo.value.length){
                case 0:
                if (tecla.search(/^[jgvpeJGVPE]$/)==-1)
                    return false;
                break;
                case 1:
                if (tecla.search(/^\d$/)!=-1)
                    campo.value=campo.value + "-" + tecla;
                return false;
                break;
                default:
                if (tecla.search(/^\d$/)==-1)
                    return false;
            }

            campo.value+=tecla.toUpperCase();
            return false;
        }



          $('input[type=radio][name=tipo_maquinas]').change(function() {
            if (this.value == 'OTRO') {
              $("#text_otro2").show();
            }
            else{
             $("#text_otro2").hide();
            }
          });


            // Smart Wizard
            $('#smartwizard').smartWizard({
                  selected: 0,  // Initial selected step, 0 = first step 
                  keyNavigation:false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                  contentCache:false,
                  autoAdjustHeight:true, // Automatically adjust content height
                  cycleSteps: false, // Allows to cycle the navigation of steps
                  backButtonSupport: true, // Enable the back button support
                  useURLhash: true, // Enable selection of the step based on url hash
                  lang: {  // Language variables
                      next: 'Siguiente', 
                      previous: 'Atras'
                  },
                  toolbarSettings: {
                      toolbarPosition: 'bottom', // none, top, bottom, both
                      toolbarButtonPosition: 'right', // left, right
                      showNextButton: true, // show/hide a Next button
                      showPreviousButton: true, // show/hide a Previous button

                      toolbarExtraButtons: [
                  $('<button></button>').text('Enviar')
                                .addClass('btn btn-info enviarformulario')
                      
                                .on('click', function(){ 

                                }),
                  $('').text('')
                                .addClass('btn btn-danger')
                                .on('click', function(){ 
                              alert('Cancel button click');                            
                                })
                            ]
                  }, 
                  anchorSettings: {
                      anchorClickable: true, // Enable/Disable anchor navigation
                      enableAllAnchors: false, // Activates all anchors clickable all times
                      markDoneStep: true, // add done css
                      enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                  },            
                  contentURL: null, // content url, Enables Ajax content loading. can set as data data-content-url on anchor
                  disabledSteps: [],    // Array Steps disabled
                  errorSteps: [],    // Highlight step with errors
                  theme: 'arrows',
                  transitionEffect: 'fade', // Effect on navigation, none/slide/fade
                  transitionSpeed: '400'
            });

          
            $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
                var elmForm = $("#form-step-" + stepNumber);

                var env = $("#enviar").val();

                var url = $(location).attr('href').split("/").splice(6, 9).join("/");
                var url2 = url.split('#')[1].substring(5, 6);

                if(stepDirection === 'forward' && elmForm){
                    elmForm.validator('validate');
                    var elmErr = elmForm.children('.has-error');
                    if(elmErr && elmErr.length > 0){
                        // Form validation failed
                        return false;
                    }
                }
                return true;
            });



            //dataTable
            $('#example').DataTable( {
                "initComplete": function(settings, json) {
                    $("#example").removeClass( "display" );
                },
                dom: 'Bfrtip',
                order:[1,"desc"],
                buttons: [
                ],
                language: {
                    search: "Buscar",
                    paginate: {
                        first:      "Primero",
                        previous:   "Anterior",
                        next:       "Siguiente",
                        last:       "Último"
                    },
                    info:           "",
                    scrollY: 1300,
                    "lengthMenu":     "_MENU_"
                }
            });


        $('#example tbody').on( 'click', 'button', function () {
            var table =  $('#example').DataTable();
            var id=$(this).attr('id');
            var idusuario = jQuery(this).attr("id");
            var rows = $(this).parents('tr')
              alertify.confirm("¿Esta seguro que desea eliminar este usuario?",
                function (e) {
                    if (e) {
                        $.ajax({
                            url: '<?php echo get_site_url('usuarios/usuarios/delete/')?>',
                            type: 'get',
                            data: { 'id' : idusuario},
                            success: function (data) {
                                console.log(data);
                                alertify.success('Usuario Eliminado');
                                table.row(rows).remove().draw();
                            },error: function (err) {
                                console.log(err);
                            }
                        });
                        return false;
                    } else {
                        alertify.error("Usted ha cancelado la solicitud");
                    }
                },
                function () {
                    var error = alertify.error('Cancel');
                });        
         });


         //dataTable
         $('#censo').DataTable( {
            "initComplete": function(settings, json) {
                $("#censo").removeClass( "display" );
            },
            dom: 'Bfrtip',
            order:[1,"desc"],
            buttons: [
            ],
            language: {
                search: "Buscar",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Último"
                },
                info:           "",
                scrollY: 1300,
                "lengthMenu":     "_MENU_"
            }
        });

           $('#censo tbody').on( 'click', 'button', function () {
                var table =  $('#censo').DataTable();
                var id=$(this).attr('id');
                var idusuario = jQuery(this).attr("id");
                var rows = $(this).parents('tr')
                alertify.confirm("¿Esta seguro que desea eliminar este grupo?",
                    function (e) {
                        if (e) {
                            $.ajax({
                                url: '<?php echo get_site_url('contactos/contactos/delete/')?>',
                                type: 'get',
                                data: { 'id' : idusuario},
                                success: function (data) {
                                    console.log(data);
                                    alertify.success('Grupo Eliminado');
                                    table.row(rows).remove().draw();
                                },error: function (err) {
                                    console.log(err);
                                }
                            });
                            return false;
                        } else {
                            alertify.error("Usted ha cancelado la solicitud");
                        }
                    },
                    function () {
                        var error = alertify.error('Cancel');
                    });
            });

            //campanas
            $('#campanas').DataTable( {
                "initComplete": function(settings, json) {
                    $("#campanas").removeClass( "display" );
                },
                dom: 'Bfrtip',
                order:[1,"desc"],
                buttons: [
                ],
                language: {
                    search: "Buscar",
                    paginate: {
                        first:      "Primero",
                        previous:   "Anterior",
                        next:       "Siguiente",
                        last:       "Último"
                    },
                    info:           "",
                    scrollY: 1300,
                    "lengthMenu":     "_MENU_"
                }
            });


        $('#campanas tbody').on( 'click', 'button', function () {
            var table =  $('#campanas').DataTable
            var id=$(this).attr('id');
            var idusuario = jQuery(this).attr("id");
            var rows = $(this).parents('tr')

              alertify.confirm("¿Esta seguro que desea eliminar esta campaña?",
                function (e) {
                    if (e) {
                        $.ajax({
                            url: '<?php echo get_site_url('campanas/campanas/delete/')?>',
                            type: 'get',
                            data: { 'id' : idusuario},
                            success: function (data) {
                                console.log(data);
                                alertify.success('Campanas Eliminado');
                                table.row(rows).remove().draw();
                            },error: function (err) {
                                console.log(err);
                            }
                        });
                        return false;
                    } else {
                        alertify.error("Usted ha cancelado la solicitud");
                    }
                },
                function () {
                    var error = alertify.error('Cancel');
                });
         });


        $('.btn-edit-contacto').click(function(e) {
            e.preventDefault();
            var idcontacto = $(this).attr("id");
            var url = '<?php echo get_site_url('contactos/contactos/getcontactos/')?>';
            $.ajax({
                url: url,
                type: 'get',
                data: { 'id' : idcontacto},
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    $("#nombre_contacto").val(obj.nombre_contacto);
                    $("#telefono_contacto").val(obj.telefono_contacto);
                    $("#rif_contacto").val(obj.rif_contacto);
                    $("#idcontacto").val(obj.idcontacto);
                    $("#grupo_contacto").val(obj.grupo_contacto);
                },error: function (err) {
                    console.log(err);
                }
            });
        });
        $('#edit-contact').click(function(e) {
            e.preventDefault();
            var form = $("#edit_contact");
            var url = form.attr('action');

            alertify.confirm("¿Esta seguro que desea modificar esta usuario?",
                function (e) {
                    if (e) {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(), 
                            success: function(data){
                                console.log(data);
                                alertify.success('Usuario Modificado');
                                location.reload();
                            }
                        });
                        return false;
                    } else {
                        alertify.error("Usted ha cancelado la solicitud");
                    }
                },
                function () {
                    var error = alertify.error('Cancel');
                });
        });

        //dataTable
        $('#lista_contactos').DataTable({
            "initComplete": function(settings, json) {
                $("#lista_contactos").removeClass( "display" );
            },
            dom: 'Bfrtip',
            order:[1,"desc"],
            buttons: [
            ],
            language: {
                search: "Buscar",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Último"
                },
                info:           "",
                scrollY: 1300,
                "lengthMenu":     "_MENU_"
            }
        });


        $('#lista_contactos tbody').on( 'click', 'button', function () {
      
        var table =  $('#lista_contactos').DataTable();
        var id=$(this).attr('id');
        var idcontacto = jQuery(this).attr("id");
        var rows = $(this).parents('tr')

          alertify.confirm("¿Esta seguro que desea eliminar este contacto?",
            function (e) {
                if (e) {
                    $.ajax({
                        url: '<?php echo get_site_url('contactos/contactos/delete_contact/')?>',
                        type: 'get',
                        data: { 'id' : idcontacto},
                        success: function (data) {
                            console.log(data);
                            alertify.success('Contacto Eliminado');
                            table.row(rows).remove().draw();
                        },error: function (err) {
                            console.log(err);
                        }
                    });
                    return false;
                } else {
                    alertify.error("Usted ha cancelado la solicitud");
                }
            },
            function () {
                var error = alertify.error('Cancel');
            });
         } );


        }); 
      </script>

  </body>
</html>