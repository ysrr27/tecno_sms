   <!-- page content -->
     <div class="right_col" role="main">
          <div class="">

            <div class="row top_tiles">
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-bullhorn"></i></div>
                  <div class="count"><?php echo $count[0]->num; ?></div>
                  <h3>Campañas</h3>
                </div>
              </div>
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-users"></i></div>
                  <div class="count"><?php echo $count[0]->num; ?></div>
                  <h3>Grupo de Contactos</h3>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">                                                
                    <h3><small><i class="fa fa-th-large"></i> Ultimas campañas ejecutadas</small></h3>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <?php 
                        if(is_array($data->rows )){
                                foreach($data->rows as $key =>$row){ ?>

                                    <article class="media event">
                                        <a class="pull-left date">
                                          <p class="month"><?php
                                          $mydate = $row->ejecucion_camapana;
                                          $resultado = str_replace("/", "-", $mydate);
                                          $month = date("M", strtotime($resultado));
                                          echo $month;
                                           ?></p>
                                          <p class="day"><?php
                                          $mydate = $row->ejecucion_camapana;
                                          $resultado = str_replace("/", "-", $mydate);
                                          $day = date("d", strtotime($resultado));
                                          echo $day;
                                           ?></p>
                                        </a>
                                        <div class="media-body">
                                          <a class="title" href="<?php echo get_site_url('campanas/campanas/preview/'.$row->idcampana)?>" style="color: #6A6A6A;"><?php echo $row->nombre_campana?></a>
                                          <p><b>Ejecución: </b><?php echo $row->ejecucion_camapana; ?></p> 
                                          <p><b>Mensaje enviados: </b><?php echo $row->sms_enviados; ?></p>
                                        </div>
                                      </article>
                                            <?php
                                        }
                            }
                    ?>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                     <h3><small><i class="fa fa-th-large"></i> Campañas pendintes por ejecutar</small></h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <?php 
                        if(is_array($data->rowspendientes )){
                                foreach($data->rowspendientes as $key =>$row){ ?>
                                    <article class="media event">
                                        <a class="pull-left date">
                                          <p class="month"><?php
                                          $mydate = $row->ejecucion_camapana;
                                          $resultado = str_replace("/", "-", $mydate);
                                          $month = date("M", strtotime($resultado));
                                          echo $month;
                                           ?></p>
                                          <p class="day"><?php
                                          $mydate = $row->ejecucion_camapana;
                                          $resultado = str_replace("/", "-", $mydate);
                                          $day = date("d", strtotime($resultado));
                                          echo $day;
                                           ?></p>
                                        </a>
                                        <div class="media-body">
                                          <a class="title" href="<?php echo get_site_url('campanas/campanas/preview/'.$row->idcampana)?>" style="color: #6A6A6A;"><?php echo $row->nombre_campana?></a>
                                          <p><b>Ejecución: </b><?php echo $row->ejecucion_camapana; ?></p> 
                                          <p><b>Mensaje enviados: </b><?php echo $row->sms_enviados; ?></p>
                                        </div>
                                      </article>
                                            <?php
                                        }
                            }
                    ?>
                  </div>
                </div>
              </div>
            </div>
           </div>
          </div>
        <!-- /page content -->