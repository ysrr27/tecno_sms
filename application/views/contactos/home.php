
 <style type="text/css">
     .display{
          display: none;
     }
</style>

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                        <a>Home</a>
                        <a>Contactos</a>
                        Lista de grupos
                   </div>
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div>
                  <div id="loading-wrapper">
                    <div id="loading-text">Cargando...</div>
                    <div id="loading-content"></div>
                </div>
                  <div class="x_content">

                  <table id="censo" class="row-border display" style="width:100%">
                      <thead>
                          <tr>
                              <th>Acción</th>
                              <th>Grupo</th>
                              <th>Contactos</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php 
                      

                        if(is_array($data->rows )){
                          foreach(  $data->rows as $key =>$row){
                                    ?>
                                  <tr>
                                      <td>
                                        <div class="btn-group  btn-group-sm">
                                          <a href="<?php echo get_site_url('contactos/contactos/preview/'.$row->idgrupo)?>" class="btn btn-default sw-btn-next" type="button" title=”VER”><i class="fa fa-eye"></i></a>
                                          <button class="btn btn-xs btn-default btn-action" name="Activar" data-toggle="tooltip" title=”Borrar” data-placement="right" id="<?php echo  $row->idgrupo?>"><i class="fa fa-times"></i></button> 
                                        </div>
                                      </td>
                                      <td><?php echo  $row->nombre_grupo ?></td>
                                      <td><?php echo  $row->count ?></td>
                                
                                  </tr>
                                  <?php

                                }
                              }                          
                        ?>
                      </tbody>
                      <tfoot>
                          <tr>
                             <td colspan="3"></td>
                          </tr>
                      </tfoot>
                  </table>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

