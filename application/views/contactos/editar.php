<!-- page content -->


<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="breadcrumb">
                    <a>Home</a>Grupo de contactos
                </div>
                <div class="x_panel">
                    <div class="x_content">

                        <div class="x_title">
                            <h3><small><i class="fa fa-th-large"></i> GRUPO DE CONTACTOS</small></h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="panel-body">

                                <?php if (!empty($err)) {
                                    echo '<div class="alert alert-danger alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    <strong>'.$err.'</strong>
                                    </div>';
                                }



                                 ?>

                                <div class="outer-container">
                                    <form action="<?php echo get_site_url("contactos/contactos/importcontacto")?>" method="post" enctype="multipart/form-data">
                                        <div>
                                            <label><i class="fa fa-cloud-upload"></i> Elija Archivo Excel</label> 
                                            <input type="file" name="uploadFile"
                                            id="uploadFile" accept=".xls,.xlsx">
                                            <input type="submit" class="btn btn-submit" name="submit" value="Importar Registros" />

                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->