<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                        <a>Home</a>
                        Grupo de contactos
                   </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small><i class="fa fa-list-alt"></i> Grupo de contactos: " <?php echo  $data[0]->grupo_contacto ?> "</small></h2>
                      
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  	<div class="row">
                  		<!-- NEW COL START -->
                  		<article class="col-sm-12 col-md-12 col-lg-12">
                        <fieldset>
                          <div class="row">
                          <div class="x_title">
                            <div class="row">
                        <table id="lista_contactos" class="row-border display" style="width:100%">
                      <thead>
                          <tr>
                              <th>Acción</th>
                              <th>Nombre</th>
                              <th>Teléfono</th>
                              <th>Rif</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php 
                        if(is_array($data)){
                          foreach(  $data as $key =>$row){
                                    ?>
                                  <tr>
                                    <td>
                                        <div class="btn-group  btn-group-sm">                   
                                          <a href="#myModal" class="btn-solicitud btn btn-default sw-btn-next btn-edit-contacto" type="button" data-toggle="modal" data-target="#myModal"  id="<?php echo  $row->idcontacto ?>"><i class="fa fa-edit"></i></a>
                                          <button class="btn btn-xs btn-default btn-action btn-delete" name="Activar" data-toggle="tooltip" title=”Borrar” data-placement="right" id="<?php echo  $row->idcontacto?>"><i class="fa fa-times"></i></button> 
                                        </div>
                                      </td>
                                      <td><?php echo  $row->nombre_contacto ?></td>
                                      <td><?php echo  $row->telefono_contacto ?></td>
                                      <td><?php echo  $row->rif_contacto ?></td>
                                  </tr>
                                  <?php

                                }
                              }                          
                        ?>
                      </tbody>
                      <tfoot>
                          <tr>
                             <td colspan="4"></td>
                          </tr>
                      </tfoot>
                  </table>
                            </div>
                            <div style="clear: both;"></div>
                          </div>
                          </div>
                        </fieldset>
                  		</article>
                  	</div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->



        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><small><i class="fa fa-list-alt"></i> Grupo de contactos: " <?php echo  $data[0]->grupo_contacto ?> "</small></h4>
                    </div>
                    <form action="<?php echo get_site_url("contactos/contactos/update_contact")?>" id="edit_contact" role="form" data-toggle="validator" name="nuevo_mensaje" method="post" accept-charset="utf-8">
                        <div class="modal-body row">
                            <div class="col-md-12 col-sm-10 col-xs-12 form-group">
                                <label>Nombre y Apellido</label>
                                <input type="text" class="form-control" name="nombre_contacto" id="nombre_contacto" value=""/>
                                <input type="hidden" class="form-control" name="idcontacto" id="idcontacto" value=""/>
                                <input type="hidden" class="form-control" name="grupo_contacto" id="grupo_contacto" value=""/>
                            </div>
                            <div class="col-md-12 col-sm-10 col-xs-12 form-group">
                                <label>Teléfono</label>
                                <input type="text" class="form-control" name="telefono_contacto" id="telefono_contacto" value=""/>
                            </div>
                            <div class="col-md-12 col-sm-10 col-xs-12 form-group">
                                <label>Rif</label>
                                <input type="text" class="form-control" name="rif_contacto" id="rif_contacto" value=""/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button class="btn btn-success sw-btn-next" id="edit-contact">
                                <i class="fa fa-save"></i> Guardar</button>      
                            </div>
                        </div>
                    </form>
                </div>
            </div>