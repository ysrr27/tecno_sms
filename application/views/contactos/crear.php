<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="breadcrumb">
                    <a>Home</a>Contacto
                </div>
                <div class="x_panel">
                    <div class="x_content">
                        <form class="form-horizontal form-label-left input_mask" action="<?php echo get_site_url("contactos/contactos/update_contacto")?>" method="post">  
                            <input type="hidden" id="idgrupo" name="idgrupo" value="<?php echo (!empty($data->idgrupo) && $data->idgrupo > '') ? $data->idgrupo : ''; ?>">

                            <div class="x_title">
                                <h3><small><i class="fa fa-th-large"></i> CREAR CONTACTO</small></h3>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="panel-body">
                                    <div class="col-md-6 col-sm-10 col-xs-12 form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span>
                                            <input type="text" class="form-control" name="nombre_contacto" id="nombre_contacto" placeholder="Nombre" value="<?php echo (!empty($data->nombre_contacto) && $data->nombre_contacto > '') ? $data->nombre_contacto : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-10 col-xs-12 form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-menu-hamburger"></i>
                                            </span>
                                            <input type="text" class="form-control" name="apellido_contacto" id="apellido_contacto" placeholder="Apellido"  value="<?php echo (!empty($data->apellido_contacto) && $data->apellido_contacto > '') ? $data->apellido_contacto : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-10 col-xs-12 form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-phone"></i>
                                            </span>
                                            <input type="text" class="form-control" name="telefono_contacto" id="telefono_contacto" placeholder="Teléfono" value="<?php echo (!empty($data->telefono_contacto) && $data->telefono_contacto > '') ? $data->telefono_contacto : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-10 col-xs-12 form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-envelope"></i>
                                            </span>
                                            <input type="text" class="form-control" name="correo_contacto" id="correo_contacto" placeholder="Correo"  value="<?php echo (!empty($data->correo_contacto) && $data->correo_contacto > '') ? $data->correo_contacto : ''; ?>">
                                        </div>
                                    </div>                              
                                    <br><br><br><br><br><br>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-5">
                                          <button type="submit" class="btn btn-success sw-btn-next"><i class="fa fa-save"></i> Guardar</button>
                                      </div>
                                  </div>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /page content -->