<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb">
                        <a>Home</a>
                        <a>Censo</a>
                        Crear
                   </div>
                <div class="x_panel">
                  <div id="loading-wrapper">
                    <div id="loading-text">Cargando...</div>
                    <div id="loading-content"></div>
                  </div>
                  <div class="x_content">


                    <!-- Smart Wizard -->
      <form action="<?php echo get_site_url("censo/censo/update")?>" id="myForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
        <!-- SmartWizard html -->

        <?php 
 $id = $this->uri->segment(4);
 if($id > 0){
        $var = 'done';
      }else{
        $var = '';
      } 
 ?>
        <div id="smartwizard" class="form_wizard wizard_verticle">
          <ul>
               <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                    <a href="#step-1"><small>IDENTIFICACIÓN <br> DEL <br>CONTRIBUYENTE</small></a>
               </li>
               <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                    <a href="#step-2"><small>ACTIVIDADES <br> ECONOMICAS <br><br></small></a>
               </li>
               <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                    <a href="#step-3"><small>INMUEBLES <br> URBANOS<br><br></small></a>
               </li>
               <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                    <a href="#step-4"><small>


          LICENCIA PARA <br> EL EXPENDIO <br> DE LICORES</small></a>
               </li>
               <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                    <a href="#step-5"><small>APUESTAS <br> LICITAS, PUBLICIDAD  <br><br></small></a>
               </li>
               <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>"><a href="#step-6"><small>DATOS <br> PERSONALES <br>OBSERVACIONES<br></small></a>
          </li>
     </ul>

            <div>
                 <div id="step-1">
                    <div id="form-step-0" role="form" data-toggle="validator">
                         <br>
                         <input type="hidden" class="form-control" name="idcenso" id="idcenso" value="<?php echo (!empty($data->idcenso) && $data->idcenso > '') ? $data->idcenso : ''; ?>">
                         <input type="hidden" class="form-control" name="idcontribuyente" id="idcontribuyente" value="<?php echo (!empty($data->idcontribuyente) && $data->idcontribuyente > '') ? $data->idcontribuyente : ''; ?>">
                         <input type="hidden" class="form-control" name="idactividad" id="idactividad" value="<?php echo (!empty($data->idactividad) && $data->idactividad > '') ? $data->idactividad : ''; ?>">
                         <input type="hidden" class="form-control" name="idinmueble" id="idinmueble" value="<?php echo (!empty($data->idinmueble) && $data->idinmueble > '') ? $data->idinmueble : ''; ?>">
                         <input type="hidden" class="form-control" name="idexpendio" id="idexpendio" value="<?php echo (!empty($data->idexpendio) && $data->idexpendio > '') ? $data->idexpendio : ''; ?>">
                         <input type="hidden" class="form-control" name="idpublicidad" id="idpublicidad" value="<?php echo (!empty($data->idpublicidad) && $data->idpublicidad > '') ? $data->idpublicidad : ''; ?>">
                         <input type="hidden" class="form-control" name="idapuestas" id="idapuestas" value="<?php echo (!empty($data->idapuestas) && $data->idapuestas > '') ? $data->idapuestas : ''; ?>">
                         <input type="hidden" class="form-control" name="iddetalles" id="iddetalles" value="<?php echo (!empty($data->iddetalles) && $data->iddetalles > '') ? $data->iddetalles : ''; ?>">
                         <input type="hidden" class="form-control" name="idservicio" id="idservicio" value="<?php echo (!empty($data->idservicio) && $data->idservicio > '') ? $data->idservicio : ''; ?>">
                         <input type="hidden" class="form-control" name="iddatos" id="iddatos" value="<?php echo (!empty($data->iddatos) && $data->iddatos > '') ? $data->iddatos : ''; ?>">

                       

                        <div class="form-group row">
                             <div class="col-md-6">
                                 <label for="razon_social">Razón Social:</label>
                                 <input type="text" class="form-control" name="razon_social" id="razon_social" placeholder="" value="<?php echo (!empty($data->razon_social) && $data->razon_social > '') ? $data->razon_social : ''; ?>" required>
                                 <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-md-6">
                                   <label for="denominacion_comercial">Denominación Comercial:</label>
                                   <input type="text" class="form-control" name="denominacion_comercial" id="denominacion_comercial" placeholder="" value="<?php echo (!empty($data->denominacion_comercial) && $data->denominacion_comercial > '') ? $data->denominacion_comercial : ''; ?>" required>
                                   <div class="help-block with-errors"></div> 
                              </div>      
                        </div>
                        <div class="form-group row">
                             <div class="col-md-6">
                                   <label for="rif">Rif:</label>
                                  </div>
                         </div>
                         <div class="form-group row">
                            <div class="col-md-12">

                                <div class="col-md-12 input-group" style="float:left;">

                                    <div class="col-md-12 input-group" style="float:left;">
                                        <input class="form-control required formatoRif" data-partner-id="rif" id="rif" name="rif"  type="text" 
                                        value="<?php echo (!empty($data->rif) && $data->rif > '') ? $data->rif : ''; ?>" placeholder="J-00000000-0"/>
                                        <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                    </div>
                                    <p class="center-align">
                                        <small><b>Ejemplo: J-00000000-0</b>, ingrese la letra (J, G, V, E, P) al inicio, no debe colocar los guiones solo los numeros.</small>
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="local">Local:</label>
                              <br>
                                 <fieldset>
                                   <div class="radiobutton">
                                        <input type="radio" name="local" id="GRANDE" value="GRANDE" <?php echo (!empty($data->local) && $data->local == 'GRANDE') ? 'checked' : '' ; ?>/>
                                        <label for="GRANDE">GRANDE</label>
                                   </div>
                                   <div class="radiobutton">
                                        <input type="radio" name="local" id="MEDIANO" value="MEDIANO" <?php echo (!empty($data->local) && $data->local == 'MEDIANO') ? 'checked' : ''; ?>  />
                                        <label for="MEDIANO">MEDIANO</label>
                                   </div>
                                   <div class="radiobutton">
                                        <input type="radio" name="local" id="PEQUEÑO" value="PEQUEÑO" <?php echo (!empty($data->local) && $data->local == 'PEQUEÑO') ? 'checked' : ''; ?>  />
                                        <label for="PEQUEÑO">PEQUEÑO</label>
                                   </div>
                              </fieldset>
                            </div>
                            <div class="col-md-3">  
                              <label for="medidas">Medidas:</label>
                              <input type="text" class="form-control" name="medidas" id="medidas" placeholder="" value="<?php echo (!empty($data->medidas) && $data->medidas > '') ? $data->medidas : ''; ?>" required>
                              <div class="help-block with-errors"></div>
                              </div>
                              <div class="col-md-3">
                                   <label for="uso">Uso:</label>
                                   <input type="text" class="form-control" name="uso" id="uso" placeholder="" value="<?php echo (!empty($data->uso) && $data->uso > '') ? $data->uso : ''; ?>" required>
                                   <div class="help-block with-errors"></div>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-md-4">
                                   <label for="avenida_calle">Avenida / Calle:</label>
                                   <input type="text" class="form-control" name="avenida_calle" id="avenida_calle" placeholder="" value="<?php echo (!empty($data->avenida_calle) && $data->avenida_calle > '') ? $data->avenida_calle : ''; ?>" required>
                                   <div class="help-block with-errors"></div>
                              </div> 
                              <div class="col-md-8">
                                 <label for="punto_referencia">Punto de referencia:</label>
                                 <input type="text" class="form-control" name="punto_referencia" id="punto_referencia" placeholder="" value="<?php echo (!empty($data->punto_referencia) && $data->punto_referencia > '') ? $data->punto_referencia : ''; ?>" required>
                                 <div class="help-block with-errors"></div>
                            </div>    
                         </div>
                        <div class="form-group row">
                         <div class="col-md-4">
                                   <label for="edif_quinta">Edif. / Quinta / Casa:</label>
                                   <input type="text" class="form-control" name="edif_quinta" id="edif_quinta" placeholder=""  value="<?php echo (!empty($data->edif_quinta) && $data->edif_quinta > '') ? $data->edif_quinta : ''; ?>" required>
                                   <div class="help-block with-errors"></div>
                              </div> 
                              <div class="col-md-4">
                                   <label for="piso">Piso:</label>
                                   <input type="text" class="form-control" name="piso" id="piso" placeholder="" value="<?php echo (!empty($data->piso) && $data->piso > '') ? $data->piso : ''; ?>" required>
                                   <div class="help-block with-errors"></div>
                              </div> 
                         </div>

                         <div class="row">
                            <div class="col-md-4">
                              <div class="col-md-12">
                                   <label for="telefono_1">Teléfono:</label>
                                   </div>
                                   <div class="col-md-5">
                                      <select id="cod_telefono_1" name="cod_telefono_1" class="form-control" >
                                       <?php echo (!empty($data->cod_telefono_1) && $data->cod_telefono_1 > '') ? "<option value='".$data->cod_telefono_1."' selected>".$data->cod_telefono_1."</option>" : ''; ?>
                                       <option value="">Codigo</option>
                                       <option value="0212">0212</option> 
                                       <option value="0426">0426</option> 
                                       <option value="0416">0416 </option> 
                                       <option value="0424">0424</option>
                                       <option value="0414">0414</option> 
                                       <option value="0412">0412</option> 

                                  </select> 
                                   </div>
                                   <div class="col-md-7 form-group">
                                     <input type="text" class="form-control" name="telefono_1" id="telefono_1" placeholder="" data-inputmask="'mask' : '999-9999'" value="<?php echo (!empty($data->telefono_1) && $data->telefono_1 > '') ? $data->telefono_1 : ''; ?>" required>
                                   <div class="help-block with-errors"></div>
                                 </div>
                                   <div class="col-md-12">
                                   <p class="center-align">
                                     <small><b>Ejemplo: 0412-999-9999</b>, No debe colocar los guiones solo los numeros.</small>
                                   </p>
                                   </div>
                                   
                              </div> 
                               <div class="col-md-8">
                                   <label for="rrss">Redes Sociales:</label>
                                   <input type="text" class="form-control" name="rrss" id="rrss" placeholder="" value="<?php echo (!empty($data->rrss) && $data->rrss > '') ? $data->rrss : ''; ?>">
                                   <div class="help-block with-errors"></div>
                              </div> 
                           
                        </div>
                        <div class="form-group row">
                             <div class="col-md-4">
                                 <label for="contrato_electricidad">Cuenta Contrato Electricidad:</label>
                                 <input type="text" class="form-control" name="contrato_electricidad" id="contrato_electricidad" data-inputmask="'mask' : '99999999999999'" placeholder="" value="1 <?php echo (!empty($data->contrato_electricidad) && $data->contrato_electricidad > '') ? $data->contrato_electricidad : ''; ?>"required>
                                 <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-md-4">
                              <label for="e_mail">Correo Electronico:</label>
                              <input type="text" class="form-control" name="e_mail" id="e_mail" placeholder="" data-inputmask-alias="email" data-val="true" data-val-required="Required" value="<?php echo (!empty($data->e_mail) && $data->e_mail > '') ? $data->e_mail : ''; ?>" required>
                              <div class="help-block with-errors"></div>
                              <p class="center-align">
                                   <small><b>Ejemplo: correo@correo.com</b></small>
                              </p>
                            </div>
                             <div class="col-md-4">
                                 <label for="tipo_negocio">Tipo de Negocio:</label>
                                 <input type="text" class="form-control" name="tipo_negocio" id="tipo_negocio" placeholder="" value=" <?php echo (!empty($data->tipo_negocio) && $data->tipo_negocio > '') ? $data->tipo_negocio : ''; ?>"required>
                                 <div class="help-block with-errors"></div>
                            </div> 
                             
                        </div>

                    </div>
                </div>
                <div id="step-2">
                   <div id="form-step-1" role="form" data-toggle="validator">
                     <br>
                     <div class="form-group row">
                             <div class="col-md-12">
                                 <label for="num_cuenta_renta">Número de cuenta renta:</label>
                                 <input type="text" class="form-control" name="num_cuenta_renta" id="num_cuenta_renta" placeholder="" value="<?php echo (!empty($data->num_cuenta_renta) && $data->num_cuenta_renta > '') ? $data->num_cuenta_renta : ''; ?>"  required>
                                 <div class="help-block with-errors"></div>
                            </div>
                             
                    </div>
                    <div class="row">
                          <div class="col-md-6">
                              <br>
                                 <fieldset>
                                   <label for="lic_a_e">Licencia de Actividades Económicas:</label>
                                   <div class="radiobutton">
                                        <input type="radio" name="lic_a_e" id="si_lic_a_e" value="SI" <?php echo (!empty($data->lic_a_e) && $data->lic_a_e == 'SI') ? 'checked' : ''; ?> />
                                        <label for="si_lic_a_e">Si</label>
                                   </div>
                                   <div class="radiobutton">
                                        <input type="radio" name="lic_a_e" id="no_lic_a_e" value="NO" <?php echo (!empty($data->lic_a_e) && $data->lic_a_e == 'NO') ? 'checked' : ''; ?>  />
                                        <label for="no_lic_a_e">No</label>
                                   </div>
                              </fieldset>
                            </div>  
                              <div class="col-md-3">
                              <br>
                                 <fieldset>
                                   <label for="exhibida">Exibida:</label>
                                   <div class="radiobutton">
                                        <input type="radio" name="exhibida" id="si_exhibida" value="SI" <?php echo (!empty($data->exhibida) && $data->exhibida == 'SI') ? 'checked' : ''; ?> />
                                        <label for="si_exhibida">Si</label>
                                   </div>
                                   <div class="radiobutton">
                                        <input type="radio" name="exhibida" id="no_exhibida" value="NO" <?php echo (!empty($data->exhibida) && $data->exhibida == 'NO') ? 'checked' : ''; ?>  />
                                        <label for="no_exhibida">No</label>
                                   </div>
                              </fieldset>
                            </div> 

                             <div class="col-md-2">
                                <label for="anio_licencia">Año:</label>
                                  <select id="anio_licencia" name="anio_licencia" class="form-control" >
                                       <?php echo (!empty($data->anio_licencia) && $data->anio_licencia > '') ? "<option value='".$data->anio_licencia."' selected>".$data->anio_licencia."</option>" : ''; ?>
                                       <option value="">Seleccione</option> 
                                       <option value="2019">2019</option> 
                                       <option value="2018">2018</option> 
                                       <option value="2017">2017</option> 
                                       <option value="2016">2016</option> 
                                       <option value="2015">2015</option> 
                                       <option value="2014">2014</option> 
                                       <option value="2013">2013</option> 
                                       <option value="2012">2012</option> 
                                       <option value="2011">2011</option> 
                                       <option value="2010">2010</option> 
                                       <option value="2009">2009</option>
                                       <option value="2008">2008</option>
                                       <option value="2007">2007</option>
                                       <option value="2006">2006</option>
                                       <option value="2005">2005</option>
                                       <option value="2004">2004</option>
                                       <option value="2003">2003</option>
                                       <option value="2002">2002</option>
                                       <option value="2001">2001</option>
                                       <option value="2000">2000</option>
                                       <option value="1999">1999</option>
                                       <option value="1998">1998</option>
                                       <option value="1997">1997</option>
                                       <option value="1996">1996</option>
                                       <option value="1995">1995</option>
                                       <option value="1994">1994</option>
                                       <option value="1993">1993</option>
                                       <option value="1992">1992</option>
                                       <option value="1991">1991</option>
                                       <option value="1990">1990</option>
                                       <option value="1989">1989</option>
                                       <option value="1988">1988</option>
                                       <option value="1987">1987</option>
                                       <option value="1986">1986</option>
                                       <option value="1985">1985</option>
                                       <option value="1984">1984</option>
                                       <option value="1983">1983</option>
                                       <option value="1982">1982</option>
                                       <option value="1981">1981</option>
                                       <option value="1980">1980</option>
                                       <option value="1979">1979</option>
                                       <option value="1978">1978</option>
                                       <option value="1977">1977</option>
                                       <option value="1976">1976</option>
                                       <option value="1975">1975</option>
                                       <option value="1974">1974</option>
                                       <option value="1973">1973</option>
                                       <option value="1972">1972</option>
                                       <option value="1971">1971</option>
                                       <option value="1970">1970</option>
                                  </select> 
                                 <div class="help-block with-errors"></div>
                            </div> 
                    </div>
                         <div class="form-group row">

                             <div class="col-md-12">
                                 <label for="licencia_AE">Nro. Liciencia de A.E. :</label>
                                 <input type="text" class="form-control" name="licencia_AE" id="licencia_AE" placeholder="" value="<?php echo (!empty($data->licencia_AE) && $data->licencia_AE > '') ? $data->licencia_AE : ''; ?>"  required>
                                 <div class="help-block with-errors"></div>
                            </div>
                        <!--      
                             <div class="col-md-4">
                              <br>
                                 <fieldset>
                                   <div class="radiobutton">
                                        <input type="radio" name="forma" id="FORMAL" value="FORMAL" <?php echo (!empty($data->forma) && $data->forma == 'FORMAL') ? 'checked' : '' ; ?> checked/>
                                        <label for="FORMAL">FORMAL</label>
                                   </div>
                                   <div class="radiobutton">
                                        <input type="radio" name="forma" id="PROVICIONAL" value="PROVICIONAL" <?php echo (!empty($data->forma) && $data->forma == 'PROVICIONAL') ? 'checked' : ''; ?>  />
                                        <label for="PROVICIONAL">PROVICIONAL</label>
                                   </div>
                              </fieldset>
                            </div>   -->
                        </div>
                     <!--    <div class="row">
                             <div class="col-md-6">
                                 <label for="nun_cuenta">Número de Cuenta:</label>
                                  <div id="resultadoBusqueda"></div>
                                 <input type="text" class="form-control search_tcontacto" name="num_cuenta_actual" id="num_cuenta_actual" placeholder="" value="<?php echo (!empty($data->num_cuenta_actual) && $data->num_cuenta_actual > '') ? $data->num_cuenta_actual : ''; ?>" onKeyUp="buscar();">
                                 <div class="help-block with-errors"></div>
                            </div>
                           <div class="col-md-6">
                                 <label for="nun_cuenta">Máquina fiscal operativa:</label>
                                  <br>
                                 <fieldset>
                                   <label for="maquina_fiscal"></label>
                                   <div class="radiobutton">
                                        <input type="radio" name="maquina_fiscal" id="si_maquina" value="SI" <?php echo (!empty($data->maquina_fiscal) && $data->maquina_fiscal == 'SI') ? 'checked' : ''; ?> />
                                        <label for="si_maquina">Si</label>
                                   </div>
                                   <div class="radiobutton">
                                        <input type="radio" name="maquina_fiscal" id="no_maquina_fiscal" value="NO" <?php echo (!empty($data->maquina_fiscal) && $data->maquina_fiscal == 'NO') ? 'checked' : ''; ?>  />
                                        <label for="no_maquina_fiscal">No</label>
                                   </div>
                              </fieldset>
                            </div> 
                        </div> -->
                        <div class="row">
                            <div class="form-group col-md-5">
                              <br>
                                 <fieldset>
                                   <label for="declaracion_AE">¿Presentó Declaración de A.E?:</label>
                                   <div class="radiobutton">
                                        <input type="radio" name="declaracion_AE" id="si_declaracion_AE" value="SI"  <?php echo (!empty($data->declaracion_AE) && $data->declaracion_AE == 'SI') ? 'checked' : ''; ?>required>
                                        <label for="si_declaracion_AE">Si</label>
                                   </div>
                                   <div class="radiobutton">
                                        <input type="radio" name="declaracion_AE" id="no_declaracion_AE" value="NO"  <?php echo (!empty($data->declaracion_AE) && $data->declaracion_AE == 'NO') ? 'checked' : ''; ?> required>
                                        <label for="no_declaracion_AE">No</label>
                                   </div>
                              </fieldset>
                            </div> 
                             <div class="col-md-2 ">
                                 <label for="mes_declaracion_AE">Mes:</label> <br>
                                 <select id="month" name="mes_declaracion_AE" id="mes_declaracion_AE" class="form-control declaracion_AE" />
                                     <?php echo (!empty($data->mes_declaracion_AE) && $data->mes_declaracion_AE > '') ? "<option value='".$data->mes_declaracion_AE."' selected>".$data->mes_declaracion_AE."</option>" : '<option selected hidden>Seleccione</option>'; ?>
                                      <option value="">Seleccione</option>
                                      <option value="ENERO">Enero</option> 
                                      <option value="FEBRERO">Febrero</option> 
                                      <option value="MARZO">Marzo</option> 
                                      <option value="ABRIL">Abril</option> 
                                      <option value="MAYO">Mayo</option> 
                                      <option value="JUNIO">Junio</option> 
                                      <option value="JULIO">Julio</option> 
                                      <option value="AGOSTO">Agosto</option> 
                                      <option value="SEPTIEMBRE">Septiembre</option> 
                                      <option value="OCTUBRE">Octubre</option> 
                                      <option value="NOVIEMBRE">Noviembre</option> 
                                      <option value="DICIEMBRE">Diciembre</option> 
                                 </select>
                                 
                            </div>
                             <div class="col-md-2 ">
                                 <label for="ano_declaracion_AE">Año:</label>
                                  <select id="year" name="ano_declaracion_AE" id="ano_declaracion_AE" class="form-control declaracion_AE" />
                                  <?php echo (!empty($data->ano_declaracion_AE) && $data->ano_declaracion_AE > '') ? "<option value='".$data->ano_declaracion_AE."' selected>".$data->ano_declaracion_AE."</option>" : ''; ?>
                                       <option value="">Seleccione</option> 
                                       <option value="2019">2019</option> 
                                       <option value="2018">2018</option> 
                                       <option value="2017">2017</option> 
                                       <option value="2016">2016</option> 
                                       <option value="2015">2015</option> 
                                       <option value="2014">2014</option> 
                                       <option value="2013">2013</option> 
                                       <option value="2012">2012</option> 
                                       <option value="2011">2011</option> 
                                       <option value="2010">2010</option> 
                                       <option value="2009">2009</option>
                                       <option value="2008">2008</option>
                                       <option value="2007">2007</option>
                                       <option value="2006">2006</option>
                                       <option value="2005">2005</option>
                                       <option value="2004">2004</option>
                                       <option value="2003">2003</option>
                                       <option value="2002">2002</option>
                                       <option value="2001">2001</option>
                                       <option value="2000">2000</option>
                                       <option value="1999">1999</option>
                                       <option value="1998">1998</option>
                                       <option value="1997">1997</option>
                                       <option value="1996">1996</option>
                                       <option value="1995">1995</option>
                                       <option value="1994">1994</option>
                                       <option value="1993">1993</option>
                                       <option value="1992">1992</option>
                                       <option value="1991">1991</option>
                                       <option value="1990">1990</option>
                                       <option value="1989">1989</option>
                                       <option value="1988">1988</option>
                                       <option value="1987">1987</option>
                                       <option value="1986">1986</option>
                                       <option value="1985">1985</option>
                                       <option value="1984">1984</option>
                                       <option value="1983">1983</option>
                                       <option value="1982">1982</option>
                                       <option value="1981">1981</option>
                                       <option value="1980">1980</option>
                                       <option value="1979">1979</option>
                                       <option value="1978">1978</option>
                                       <option value="1977">1977</option>
                                       <option value="1976">1976</option>
                                       <option value="1975">1975</option>
                                       <option value="1974">1974</option>
                                       <option value="1973">1973</option>
                                       <option value="1972">1972</option>
                                       <option value="1971">1971</option>
                                       <option value="1970">1970</option>
                                  </select> 
                            
                            </div> 
                     
                       </div>
                       <div class="form-group row">
                          <div class="form-group col-md-12">
                              <br>
                              <fieldset>
                                   <label for="exhibe_AE">Exhibe Declaración de A.E?:</label>
                                   <div class="radiobutton">
                                        <input type="radio" name="exhibe_AE" id="si_exhibe_AE" value="SI"  <?php echo (!empty($data->exhibe_AE) && $data->exhibe_AE == 'SI') ? 'checked' : ''; ?> required>
                                        <label for="si_exhibe_AE">Si</label>
                                   </div>
                                   <div class="radiobutton">
                                        <input type="radio" name="exhibe_AE" id="no_exhibe_AE" value="NO"  <?php echo (!empty($data->exhibe_AE) && $data->exhibe_AE == 'NO') ? 'checked' : ''; ?> required>
                                        <label for="no_exhibe_AE">No</label>
                                   </div>
                              </fieldset>
                         </div> 
                    </div> 

                      </div>
                </div>
                <div id="step-3">
                    <div id="form-step-2" role="form" data-toggle="validator">
                        <br>
                        <div class="form-group row">
                              <div class="col-md-5">
                                   <br>
                                   <fieldset>
                                        <label for="name">¿Presentó Solvencia de Inmueble?:</label>
                                        <div class="radiobutton">
                                             <input type="radio" name="solvencia_inmueble" id="si_solvencia_inmueble" value="SI" <?php echo (!empty($data->solvencia_inmueble) && $data->solvencia_inmueble == 'SI') ? 'checked' : ''; ?> />
                                             <label for="si_solvencia_inmueble" >Si</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="solvencia_inmueble" id="no_solvencia_inmueble" value="NO" <?php echo (!empty($data->solvencia_inmueble) && $data->solvencia_inmueble == 'NO') ? 'checked' : ''; ?>  />
                                             <label for="no_solvencia_inmueble" >No</label>
                                        </div>
                                   </fieldset>
                              </div> 
                             <div class="col-md-2">
                                <label for="ano_solvencia"></label>
                                  <select id="ano_solvencia" name="ano_solvencia" class="form-control" >
                                    <?php echo (!empty($data->ano_solvencia) && $data->ano_solvencia > '') ? "<option value='".$data->ano_solvencia."' selected>".$data->ano_solvencia."</option>" : '<option selected hidden>Seleccione</option>'; ?>
                                       <option value="2019">2019</option> 
                                       <option value="2018">2018</option> 
                                       <option value="2017">2017</option> 
                                       <option value="2016">2016</option> 
                                       <option value="2015">2015</option> 
                                       <option value="2014">2014</option> 
                                       <option value="2013">2013</option> 
                                       <option value="2012">2012</option> 
                                       <option value="2011">2011</option> 
                                       <option value="2010">2010</option> 
                                       <option value="2009">2009</option>
                                       <option value="2008">2008</option>
                                       <option value="2007">2007</option>
                                       <option value="2006">2006</option>
                                       <option value="2005">2005</option>
                                       <option value="2004">2004</option>
                                       <option value="2003">2003</option>
                                       <option value="2002">2002</option>
                                       <option value="2001">2001</option>
                                       <option value="2000">2000</option>
                                       <option value="1999">1999</option>
                                       <option value="1998">1998</option>
                                       <option value="1997">1997</option>
                                       <option value="1996">1996</option>
                                       <option value="1995">1995</option>
                                       <option value="1994">1994</option>
                                       <option value="1993">1993</option>
                                       <option value="1992">1992</option>
                                       <option value="1991">1991</option>
                                       <option value="1990">1990</option>
                                  </select> 
                                 <div class="help-block with-errors"></div>
                            </div> 
                            <div class="col-md-5">
                              <br>
                              <fieldset>
                                   <label for="inmueble">Inmueble:</label>
                                   <div class="radiobutton">
                                        <input type="radio" name="inmueble" id="si_inmueble" value="ARRENDADO" class="" <?php echo (!empty($data->inmueble) && $data->inmueble == 'ARRENDADO') ? 'checked' : ''; ?>  />
                                        <label for="si_inmueble">Arrendado</label>
                                   </div>
                                   <div class="radiobutton">
                                        <input type="radio" name="inmueble" id="no_inmueble" value="PROPIO" class="" <?php echo (!empty($data->inmueble) && $data->inmueble == 'PROPIO') ? 'checked' : ''; ?>/>
                                        <label for="no_inmueble">Propio</label>
                                   </div>
                              </fieldset>
                         </div>   
                        </div>
                        <div class="row">
                             <div class="col-md-6">
                                 <label for="datos">Datos:</label>
                                 <input type="text" class="form-control" name="datos" id="num_identificacion" placeholder="" value="<?php echo (!empty($data->datos) && $data->datos > '') ? $data->datos : ''; ?>">
                                 <div class="help-block with-errors"></div>
                            </div> 
                            <div class="col-md-6">
                                 <label for="numero_catastral">Número Catastro:</label>
                                 <input type="text" class="form-control" name="numero_catastral" id="numero_catastral" placeholder="" value="<?php echo (!empty($data->numero_catastral) && $data->numero_catastral > '') ? $data->numero_catastral : ''; ?>">
                                 <div class="help-block with-errors"></div>
                            </div>   
                        </div>
                        <div class="col-md-12">
                           <legend><i class="fa fa-car"></i> VEHÍCULOS</legend> 
                        </div>
                         <br>
                        <div class="row">
                            <div class="col-md-4">
                                   <br>
                                   <fieldset>
                                        <label for="name">¿Posee vehículos?:</label>
                                        <div class="radiobutton">
                                             <input type="radio" name="vehiculos" id="si_vehiculos" value="SI" <?php echo (!empty($data->vehiculos) && $data->vehiculos == 'SI') ? 'checked' : ''; ?> />
                                             <label for="si_vehiculos" >Si</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="vehiculos" id="no_vehiculos" value="NO" <?php echo (!empty($data->vehiculos) && $data->vehiculos == 'NO') ? 'checked' : ''; ?>  />
                                             <label for="no_vehiculos" >No</label>
                                        </div>
                                   </fieldset>
                              </div>    
                             <div class="col-md-4">
                                 <label for="placa">Placa:</label>
                                 <input type="text" class="form-control" name="placa" id="placa" placeholder="" value="<?php echo (!empty($data->placa) && $data->placa > '') ? $data->placa : ''; ?>">
                                 <div class="help-block with-errors"></div>
                            </div> 
                              <div class="col-md-4">
                                 <label for="propietario">Propietario:</label>
                                 <input type="text" class="form-control vehiculos_asociados" name="propietario" id="propietario" placeholder="" value="<?php echo (!empty($data->propietario) && $data->propietario > '') ? $data->propietario : ''; ?>">
                                 <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                                <div class="col-md-12">
                                 <label for="serial">Serial:</label>
                                 <input type="text" class="form-control serial" name="serial" id="serial" placeholder="" value="<?php echo (!empty($data->serial) && $data->serial > '') ? $data->serial : ''; ?>">
                                 <div class="help-block with-errors"></div>
                            </div>
                             
                        </div>
                    </div>
                </div>
                <div id="step-4" class="">
                     <div id="form-step-3" role="form" data-toggle="validator">
                         <br>
                         <div class="form-group row">
                              <div class="col-md-6">
                                   <br>
                                   <fieldset>
                                        <label for="licencia_licores">¿Presentó Licencia de Licores?:</label>
                                        <div class="radiobutton">
                                             <input type="radio" name="licencia_licores" id="si_licencia_licores" value="SI" <?php echo (!empty($data->licencia_licores) && $data->licencia_licores == 'SI') ? 'checked' : ''; ?>/>
                                             <label for="si_licencia_licores">Si</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="licencia_licores" id="no_licencia_licores" value="NO" <?php echo (!empty($data->licencia_licores) && $data->licencia_licores == 'NO') ? 'checked' : ''; ?>/>
                                             <label for="no_licencia_licores">No</label>
                                        </div>
                                         <div class="radiobutton">
                                             <input type="radio" name="licencia_licores" id="na_licencia_licores" value="N/A" <?php echo (!empty($data->licencia_licores) && $data->licencia_licores == 'N/A') ? 'checked' : ''; ?>/>
                                             <label for="na_licencia_licores">N/A</label>
                                        </div>
                                   </fieldset>
                              </div> 
                             <div class="col-md-2">
                                <label for="ano_licencia_lic">Año</label>
                                  <select id="ano_licencia_lic" name="ano_licencia_lic" class="form-control licencia_licores" >
                                       <?php echo (!empty($data->ano_licencia_lic) && $data->ano_licencia_lic > '') ? "<option value='".$data->ano_licencia_lic."' selected>".$data->ano_licencia_lic."</option>" : '<option selected hidden>Seleccione</option>'; ?> 
                                       <option value="2019">2019</option> 
                                       <option value="2018">2018</option> 
                                       <option value="2017">2017</option> 
                                       <option value="2016">2016</option> 
                                       <option value="2015">2015</option> 
                                       <option value="2014">2014</option> 
                                       <option value="2013">2013</option> 
                                       <option value="2012">2012</option> 
                                       <option value="2011">2011</option> 
                                       <option value="2010">2010</option> 
                                       <option value="2009">2009</option>
                                       <option value="2008">2008</option>
                                       <option value="2007">2007</option>
                                       <option value="2006">2006</option>
                                       <option value="2005">2005</option>
                                       <option value="2004">2004</option>
                                       <option value="2003">2003</option>
                                       <option value="2002">2002</option>
                                       <option value="2001">2001</option>
                                       <option value="2000">2000</option>
                                       <option value="1999">1999</option>
                                       <option value="1998">1998</option>
                                       <option value="1997">1997</option>
                                       <option value="1996">1996</option>
                                       <option value="1995">1995</option>
                                       <option value="1994">1994</option>
                                       <option value="1993">1993</option>
                                       <option value="1992">1992</option>
                                       <option value="1991">1991</option>
                                       <option value="1990">1990</option>
                                  </select> 
                                 <div class="help-block with-errors"></div>
                            </div> 
                            <div class="col-md-4">
                                   <br>
                                   <fieldset>
                                        <div class="radiobutton">
                                             <input type="radio" name="tipo_expendio" id="si_tipo_expendio" value="AL DETAL" <?php echo (!empty($data->tipo_expendio) && $data->tipo_expendio == 'AL DETAL') ? 'checked' : ''; ?> />
                                             <label for="si_tipo_expendio">Al detal</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="tipo_expendio" id="no_tipo_expendio" value="DE CONSUMO" <?php echo (!empty($data->tipo_expendio) && $data->tipo_expendio == 'DE CONSUMO') ? 'checked' : ''; ?>/>
                                             <label for="no_tipo_expendio">De consumo</label>
                                        </div>
                                         <div class="radiobutton">
                                             <input type="radio" name="tipo_expendio" id="na_tipo_expendio" value="AL MAYOR" <?php echo (!empty($data->tipo_expendio) && $data->tipo_expendio == 'AL MAYOR') ? 'checked' : ''; ?>/>
                                             <label for="na_tipo_expendio">Al mayor</label>
                                        </div>
                                   </fieldset>
                              </div> 
                         </div>
                          <div class="form-group row">
                             <div class="col-md-6">
                                     <label for="nro_licencia">Nro. Licencia:</label>
                                     <input type="text" class="form-control licencia_licores" name="nro_licencia" id="nro_licencia" placeholder="" value="<?php echo (!empty($data->nro_licencia) && $data->nro_licencia > '') ? $data->nro_licencia : ''; ?>" >
                                     <div class="help-block with-errors"></div>
                              </div>
                              <div class="col-md-6">
                                   <br>
                                   <fieldset>
                                   <label for="exhibida">Exhibida:</label>
                                        <div class="radiobutton">
                                             <input type="radio" name="licencia_exhibida" id="si_licencia_exhibida" value="SI" <?php echo (!empty($data->licencia_exhibida) && $data->licencia_exhibida == 'SI') ? 'checked' : ''; ?>/>
                                             <label for="si_licencia_exhibida">Si</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="licencia_exhibida" id="no_licencia_exhibida" value="NO" <?php echo (!empty($data->licencia_exhibida) && $data->licencia_exhibida == 'NO') ? 'checked' : ''; ?>/>
                                             <label for="no_licencia_exhibida">No</label>
                                        </div>
                                   </fieldset>
                              </div>     
                          </div>
                      </div>


                </div>
                <div id="step-5" class="">
                    <div id="form-step-4" role="form" data-toggle="validator">
                         <br>
                         <div class="form-group row">
                              <div class="col-md-6">
                                   <br>
                                   <fieldset>
                                        <label for="apuestas_licitas">Permiso de Apuestas Lícitas:</label>
                                        <div class="radiobutton">
                                             <input type="radio" name="apuestas_licitas" id="si_apuestas_licitas" value="SI" <?php echo (!empty($data->apuestas_licitas) && $data->apuestas_licitas == 'SI') ? 'checked' : ''; ?> />
                                             <label for="si_apuestas_licitas">Si</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="apuestas_licitas" id="no_apuestas_licitas" value="NO" <?php echo (!empty($data->apuestas_licitas) && $data->apuestas_licitas == 'NO') ? 'checked' : ''; ?> />
                                             <label for="no_apuestas_licitas">No</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="apuestas_licitas" id="na_apuestas_licitas" value="N/A" <?php echo (!empty($data->apuestas_licitas) && $data->apuestas_licitas == 'N/A') ? 'checked' : ''; ?> />
                                             <label for="na_apuestas_licitas">N/A</label>
                                        </div>
                                   </fieldset>
                              </div> 
                         </div>
                         <div class="form-group row">
                              <div class="col-md-6">
                                   <br>
                                   <fieldset>
                                        <label for="maquina_traganiquel">Maquina traganiqueles:</label>
                                        <div class="radiobutton">
                                             <input type="radio" name="maquina_traganiquel" id="si_maquina_traganiquel" value="SI" <?php echo (!empty($data->maquina_traganiquel) && $data->maquina_traganiquel == 'SI') ? 'checked' : ''; ?>  />
                                             <label for="si_maquina_traganiquel">Si</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="maquina_traganiquel" id="no_maquina_traganiquel" value="NO" <?php echo (!empty($data->maquina_traganiquel) && $data->maquina_traganiquel == 'NO') ? 'checked' : ''; ?> />
                                             <label for="no_maquina_traganiquel">No</label>
                                        </div>
                                   </fieldset>
                              </div>
                               <div class="col-md-6">
                                 <label for="cantidad_maquinas">Cantidad de Máquinas:</label>
                                 <input type="text" class="form-control apuestas_licitas" name="cantidad_maquinas" id="cantidad_maquinas" placeholder="" value="<?php echo (!empty($data->cantidad_maquinas) && $data->cantidad_maquinas > '') ? $data->cantidad_maquinas : ''; ?>">
                                 <div class="help-block with-errors"></div>
                              </div> 
                         </div>
                         <div class="row">
                               <div class="col-md-6">
                                  <br>
                                   <fieldset>
                                   <label for="aviso_publicitario">¿Permiso de publicidad?:</label>
                                        <div class="radiobutton">
                                             <input type="radio" name="aviso_publicitario" id="si_aviso_publicitario" value="SI" <?php echo (!empty($data->aviso_publicitario) && $data->aviso_publicitario == 'SI') ? 'checked' : ''; ?> />
                                             <label for="si_aviso_publicitario">Si</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="aviso_publicitario" id="no_aviso_publicitario"  value="NO" <?php echo (!empty($data->aviso_publicitario) && $data->aviso_publicitario == 'NO') ? 'checked' : ''; ?>/>
                                             <label for="no_aviso_publicitario">No</label>
                                        </div>
                                         <div class="radiobutton">
                                             <input type="radio" name="aviso_publicitario" id="na_aviso_publicitario" value="N/A" <?php echo (!empty($data->aviso_publicitario) && $data->aviso_publicitario == 'N/A') ? 'checked' : ''; ?>/>
                                             <label for="na_aviso_publicitario">N/A</label>
                                        </div>
                                   </fieldset>
                              </div>
                              <div class="col-md-6">
                                   <label for="cantidad_avisos">Cantidad de Avisos:</label>
                                   <input type="number" class="form-control aviso_publicitario" name="cantidad_avisos" id="cantidad_avisos" placeholder="" value="<?php echo (!empty($data->cantidad_avisos) && $data->cantidad_avisos > '') ? $data->cantidad_avisos : ''; ?>"  required>
                                   <div class="help-block with-errors"></div>
                              </div> 
                         </div>

                         <div class="row">
                              
                              <div class="col-md-9">
                                 <label for="tipo_aviso">Tipo de Avisos:</label>
                                     <fieldset>
                                        <div class="radiobutton">
                                             <input type="radio" name="tipo_aviso" id="si_tipo_aviso" value="VALLA" <?php echo (!empty($data->tipo_aviso) && $data->tipo_aviso == 'VALLA') ? 'checked' : ''; ?>/>
                                             <label for="si_tipo_aviso">VALLA</label>
                                        </div>
                                        <div class="radiobutton">
                                             <input type="radio" name="tipo_aviso" id="no_tipo_aviso"  value="TOLDO" <?php echo (!empty($data->tipo_aviso) && $data->tipo_aviso == 'TOLDO') ? 'checked' : ''; ?>/>
                                             <label for="no_tipo_aviso">TOLDO</label>
                                        </div>
                                         <div class="radiobutton">
                                             <input type="radio" name="tipo_aviso" id="na_tipo_aviso" value="PENDON" <?php echo (!empty($data->tipo_aviso) && $data->tipo_aviso == 'PENDON') ? 'checked' : ''; ?>/>
                                             <label for="na_tipo_aviso">PENDON</label>
                                        </div>
                                         <div class="radiobutton">
                                             <input type="radio" name="tipo_aviso" id="ca_tipo_aviso" value="CANEFA" <?php echo (!empty($data->tipo_aviso) && $data->tipo_aviso == 'CANEFA') ? 'checked' : ''; ?>/>
                                             <label for="ca_tipo_aviso">CANEFA</label>
                                        </div>
                                         <div class="radiobutton">
                                             <input type="radio" name="tipo_aviso" id="OT_tipo_aviso" value="OTRO" <?php echo (!empty($data->tipo_aviso) && $data->tipo_aviso == 'OTRO') ? 'checked' : ''; ?>/>
                                             <label for="OT_tipo_aviso">OTRO</label>
                                        </div>
                                         <div class="radiobutton">
                                             <input type="radio" name="tipo_aviso" id="LU_tipo_aviso" value="LUMINOSO" <?php echo (!empty($data->tipo_aviso) && $data->tipo_aviso == 'LUMINOSO') ? 'checked' : ''; ?>/>
                                             <label for="LU_tipo_aviso">LUMINOSO</label>
                                        </div>
                                   </fieldset>
                              </div>
                              <div class="col-md-3">
                                   <label for="cantidad_caras">Cantidad de Caras:</label>
                                   <input type="number" class="form-control" name="cantidad_caras" id="cantidad_caras" placeholder="" value="<?php echo (!empty($data->cantidad_caras) && $data->cantidad_caras > '') ? $data->cantidad_caras : ''; ?>"  required>
                                   <div class="help-block with-errors"></div>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-12">
                                   <label for="activ_eco_autorizadas">Actividades Económicas Autorizadas:</label>
                                   <input type="text" class="form-control" name="activ_eco_autorizadas" id="activ_eco_autorizadas" placeholder="" value="<?php echo (!empty($data->activ_eco_autorizadas) && $data->activ_eco_autorizadas > '') ? $data->activ_eco_autorizadas : ''; ?>"  required>
                                   <div class="help-block with-errors"></div>
                              </div> 
                              <div class="col-md-12">
                                   <label for="activ_eco_observadas">Actividades Económicas Observadas:</label>
                                   <input type="text" class="form-control" name="activ_eco_observadas" id="activ_eco_observadas" placeholder="" value="<?php echo (!empty($data->activ_eco_observadas) && $data->activ_eco_observadas > '') ? $data->activ_eco_observadas : ''; ?>"  required>
                                   <div class="help-block with-errors"></div>
                              </div>
                         </div>
                          <legend><i class="fa fa-file-o"></i> Observaciones</legend> 
                        <div class="row">
                              <div class="col-md-12">
                                  <textarea class="form-control" name="observaciones" id="observaciones" rows="3" placeholder=""> <?php echo (!empty($data->observaciones) && $data->observaciones > '') ? $data->observaciones : ''; ?></textarea>
                              </div> 
                         </div>

                    </div>
                </div>
                <div id="step-6" class="">
                    <div id="form-step-6" role="form" data-toggle="validator">
                          <br>
                               <legend><i class="fa fa-user"></i> Contribuyente</legend>
                        <div class="form-group row">
                             <div class="col-md-6">
                                 <label for="contribuyente">Contribuyente:</label>
                                 <input type="text" class="form-control" name="contribuyente" id="contribuyente" placeholder="" value="<?php echo (!empty($data->contribuyente) && $data->contribuyente > '') ? $data->contribuyente : ''; ?>">
                                 <div class="help-block with-errors"></div>
                            </div>
                             <div class="col-md-6">
                                 <label for="cedula_contribuyente">Cédula Nro.:</label>
                                 <input type="text" class="form-control cedula" name="cedula_contribuyente" id="cedula_contribuyente" placeholder="V-00000000" value="<?php echo (!empty($data->cedula_contribuyente) && $data->cedula_contribuyente > '') ? $data->cedula_contribuyente : ''; ?>">
                                 <div class="help-block with-errors"></div>
                                 <p class="center-align">
                               <small><b>Ejemplo: J-00000000</b>, ingrese la letra (V, E) al inicio, no debe colocar los guiones solo los numeros.</small>
                             </p>
                            </div>       
                        </div>
                        <div class="row">
                             <div class="col-md-4">
                              <div class="col-md-12">
                                   <label for="telf_fijo_contribuyente">Teléfono Fijo:</label>
                                   </div>
                                   <div class="col-md-5">
                                      <select id="cod_telf_fijo_contribuyente" name="cod_telf_fijo_contribuyente" class="form-control" >
                                       <?php echo (!empty($data->cod_telf_fijo_contribuyente) && $data->cod_telf_fijo_contribuyente > '') ? "<option value='".$data->cod_telf_fijo_contribuyente."' selected>".$data->cod_telf_fijo_contribuyente."</option>" : ''; ?>
                                       <option value="">Codigo</option>
                                       <option value="0212">0212</option> 
                                  </select> 
                                   </div>
                                   <div class="col-md-7">
                                     <input type="text" class="form-control" name="telf_fijo_contribuyente" id="telf_fijo_contribuyente" placeholder="" data-inputmask="'mask' : '999-9999'" value="<?php echo (!empty($data->telf_fijo_contribuyente) && $data->telf_fijo_contribuyente > '') ? $data->telf_fijo_contribuyente : ''; ?>" required>
                                   <div class="help-block with-errors"></div>
                                 </div>
                                   <div class="col-md-12">
                                   <p class="center-align">
                                     <small><b>Ejemplo: 0212-999-9999</b>, No debe colocar los guiones solo los numeros.</small>
                                   </p>
                                   </div>
                                   
                              </div> 
                            <div class="col-md-4">
                              <div class="col-md-12">
                                   <label for="telf_movil_contribuyente">Teléfono Movil:</label>
                                   </div>
                                   <div class="col-md-5">
                                      <select id="cod_telf_movil_contribuyente" name="cod_telf_movil_contribuyente" class="form-control" >
                                       <?php echo (!empty($data->cod_telf_movil_contribuyente) && $data->cod_telf_movil_contribuyente > '') ? "<option value='".$data->cod_telf_movil_contribuyente."' selected>".$data->cod_telf_movil_contribuyente."</option>" : ''; ?>
                                       <option value="">Codigo</option>
                                       <option value="0412">0412</option> 
                                       <option value="0414">0414</option> 
                                       <option value="0416">0416</option> 
                                       <option value="0424">0424</option> 
                                       <option value="0426">0426</option> 
                                  </select> 
                                   </div>
                                   <div class="col-md-7">
                                     <input type="text" class="form-control" name="telf_movil_contribuyente" id="telf_movil_contribuyente" placeholder="" data-inputmask="'mask' : '999-9999'" value="<?php echo (!empty($data->telf_movil_contribuyente) && $data->telf_movil_contribuyente > '') ? $data->telf_movil_contribuyente : ''; ?>" required>
                                   <div class="help-block with-errors"></div>
                                 </div>
                                   <div class="col-md-12">
                                   <p class="center-align">
                                     <small><b>Ejemplo: 0412-999-9999</b>, No debe colocar los guiones solo los numeros.</small>
                                   </p>
                                   </div>
                              </div> 
                             <div class="col-md-4">
                                 <label for="email_contribuyente">E-mail:</label>
                                 <input type="text" class="form-control" data-inputmask-alias="email" data-val="true" data-val-required="Required"
                                 name="email_contribuyente" id="email_contribuyente" placeholder="" value="<?php echo (!empty($data->email_contribuyente) && $data->email_contribuyente > '') ? $data->email_contribuyente : ''; ?>" required>
                                 <div class="help-block with-errors"></div>
                                 <p class="center-align">
                                     <small><b>Ejemplo: correo@correo.com</b></small>
                                   </p>
                            </div>        
                        </div>  
                        <legend><i class="fa fa-user"></i> Representante</legend>
                        <div class="form-group row">
                             <div class="col-md-6">
                                 <label for="reprensentante_legal">Representante Legal:</label>
                                 <input type="text" class="form-control" name="reprensentante_legal" id="reprensentante_legal" placeholder="" value="<?php echo (!empty($data->reprensentante_legal) && $data->reprensentante_legal > '') ? $data->reprensentante_legal : ''; ?>">
                                 <div class="help-block with-errors"></div>
                            </div>
                             <div class="col-md-6">
                                 <label for="cedula_representante">Cédula Nro.:</label>
                                 <input type="text" class="form-control cedula" name="cedula_representante" id="cedula_representante" placeholder="V-00000000" value="<?php echo (!empty($data->cedula_representante) && $data->cedula_representante > '') ? $data->cedula_representante : ''; ?>">
                                 <div class="help-block with-errors"></div>
                                 <p class="center-align">
                               <small><b>Ejemplo: J-00000000</b>, ingrese la letra (V, E) al inicio, no debe colocar los guiones solo los numeros.</small>
                             </p>
                            </div>       
                        </div>
                        <div class="row">
                             <div class="col-md-4">
                              <div class="col-md-12">
                                   <label for="telf_fijo_representante">Teléfono Fijo:</label>
                                   </div>
                                   <div class="col-md-5">
                                      <select id="cod_telf_fijo_representante" name="cod_telf_fijo_representante" class="form-control" >
                                       <?php echo (!empty($data->cod_telf_fijo_representante) && $data->cod_telf_fijo_representante > '') ? "<option value='".$data->cod_telf_fijo_representante."' selected>".$data->cod_telf_fijo_representante."</option>" : ''; ?>
                                       <option value="">Codigo</option>
                                       <option value="0212">0212</option> 
                                  </select> 
                                   </div>
                                   <div class="col-md-7">
                                     <input type="text" class="form-control" name="telf_fijo_representante" id="telf_fijo_representante" placeholder="" data-inputmask="'mask' : '999-9999'" value="<?php echo (!empty($data->telf_fijo_representante) && $data->telf_fijo_representante > '') ? $data->telf_fijo_representante : ''; ?>" required>
                                   <div class="help-block with-errors"></div>
                                 </div>
                                   <div class="col-md-12">
                                   <p class="center-align">
                                     <small><b>Ejemplo: 0212-999-9999</b>, No debe colocar los guiones solo los numeros.</small>
                                   </p>
                                   </div>
                                   
                              </div> 
                            <div class="col-md-4">
                              <div class="col-md-12">
                                   <label for="telf_movil_representante">Teléfono Movil:</label>
                                   </div>
                                   <div class="col-md-5">
                                      <select id="cod_telf_movil_representante" name="cod_telf_movil_representante" class="form-control" >
                                       <?php echo (!empty($data->cod_telf_movil_representante) && $data->cod_telf_movil_representante > '') ? "<option value='".$data->cod_telf_movil_representante."' selected>".$data->cod_telf_movil_representante."</option>" : ''; ?>
                                       <option value="">Codigo</option>
                                       <option value="0412">0412</option> 
                                       <option value="0414">0414</option> 
                                       <option value="0416">0416</option> 
                                       <option value="0424">0424</option> 
                                       <option value="0426">0426</option> 
                                  </select> 
                                   </div>
                                   <div class="col-md-7">
                                     <input type="text" class="form-control" name="telf_movil_representante" id="telf_movil_representante" placeholder="" data-inputmask="'mask' : '999-9999'" value="<?php echo (!empty($data->telf_movil_representante) && $data->telf_movil_representante > '') ? $data->telf_movil_representante : ''; ?>" required>
                                   <div class="help-block with-errors"></div>
                                 </div>
                                   <div class="col-md-12">
                                   <p class="center-align">
                                     <small><b>Ejemplo: 0412-999-9999</b>, No debe colocar los guiones solo los numeros.</small>
                                   </p>
                                   </div>
                              </div> 
                             <div class="col-md-4">
                                 <label for="email_reprensentante_legal">E-mail:</label>
                                 <input type="text" class="form-control" data-inputmask-alias="email" data-val="true" data-val-required="Required"
                                 name="email_reprensentante_legal" id="email_reprensentante_legal" placeholder="" value="<?php echo (!empty($data->email_reprensentante_legal) && $data->email_reprensentante_legal > '') ? $data->email_reprensentante_legal : ''; ?>" required>
                                 <div class="help-block with-errors"></div>
                                 <p class="center-align">
                                     <small><b>Ejemplo: correo@correo.com</b></small>
                                   </p>
                            </div>        
                        </div>
                        <legend><i class="fa fa-user"></i> Atendido por:</legend>    
                        <div class="row">
                             <div class="col-md-6">
                                 <label for="atendidopor">Atendido por:</label>
                                 <input type="text" class="form-control" name="atendidopor" id="atendidopor" placeholder="" value="<?php echo (!empty($data->atendidopor) && $data->atendidopor > '') ? $data->atendidopor : ''; ?>">
                            </div>
                             <div class="col-md-6">
                                 <label for="cedula_atendidopor">Cédula Nro:</label>
                                 <input type="text" class="form-control cedula" name="cedula_atendidopor" id="cedula_atendidopor" placeholder="" value="<?php echo (!empty($data->cedula_atendidopor) && $data->cedula_atendidopor > '') ? $data->cedula_atendidopor : ''; ?>" 
                                 >
                                 <p class="center-align">
                                   <small><b>Ejemplo: J-00000000</b>, ingrese la letra (V, E) al inicio, no debe colocar los guiones solo los numeros.</small>
                                 </p>

                            </div>       
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                              <div class="col-md-12">
                                   <label for="telf_fijo_atendidopor">Teléfono Fijo:</label>
                                   </div>
                                   <div class="col-md-5">
                                      <select id="cod_telf_fijo_atendidopor" name="cod_telf_fijo_atendidopor" class="form-control" >
                                       <?php echo (!empty($data->cod_telf_fijo_atendidopor) && $data->cod_telf_fijo_atendidopor > '') ? "<option value='".$data->cod_telf_fijo_atendidopor."' selected>".$data->cod_telf_fijo_atendidopor."</option>" : ''; ?>
                                       <option value="">Codigo</option>
                                       <option value="0212">0212</option> 
                                  </select> 
                                   </div>
                                   <div class="col-md-7">
                                     <input type="text" class="form-control" name="telf_fijo_atendidopor" id="telf_fijo_atendidopor" placeholder="" data-inputmask="'mask' : '999-9999'" value="<?php echo (!empty($data->telf_fijo_atendidopor) && $data->telf_fijo_atendidopor > '') ? $data->telf_fijo_atendidopor : ''; ?>">
                                   <div class="help-block with-errors"></div>
                                 </div>
                                   <div class="col-md-12">
                                   <p class="center-align">
                                     <small><b>Ejemplo: 0412-999-9999</b>, No debe colocar los guiones solo los numeros.</small>
                                   </p>
                                   </div>
                                   
                              </div> 
                            <div class="col-md-4">
                              <div class="col-md-12">
                                   <label for="telf_movil_representante">Teléfono Movíl:</label>
                                   </div>
                                   <div class="col-md-5">
                                      <select id="cod_telf_movil_atendidopor" name="cod_telf_movil_atendidopor" class="form-control" >
                                       <?php echo (!empty($data->cod_telf_movil_atendidopor) && $data->cod_telf_movil_atendidopor > '') ? "<option value='".$data->cod_telf_movil_atendidopor."' selected>".$data->cod_telf_movil_atendidopor."</option>" : ''; ?>
                                       <option value="">Codigo</option>
                                       <option value="0412">0412</option> 
                                       <option value="0414">0414</option> 
                                       <option value="0416">0416</option> 
                                       <option value="0424">0424</option> 
                                       <option value="0426">0426</option> 
                                  </select> 
                                   </div>
                                   <div class="col-md-7">
                                     <input type="text" class="form-control" name="telf_movil_atendidopor" id="telf_movil_atendidopor" placeholder="" data-inputmask="'mask' : '999-9999'" value="<?php echo (!empty($data->telf_movil_atendidopor) && $data->telf_movil_atendidopor > '') ? $data->telf_movil_atendidopor : ''; ?>">
                                 </div>
                                   <div class="col-md-12">
                                   <p class="center-align">
                                     <small><b>Ejemplo: 0412-999-9999</b>, No debe colocar los guiones solo los numeros.</small>
                                   </p>
                                   </div>
                              </div> 
                             <div class="col-md-4">
                                 <label for="email_atendidopor">E-mail:</label>
                                 <input type="text" class="form-control" name="email_atendidopor" id="email_atendidopor" data-inputmask-alias="email" data-val="true" data-val-required="Required"
                                 placeholder="" value="<?php echo (!empty($data->email_atendidopor) && $data->email_atendidopor > '') ? $data->email_atendidopor : ''; ?>" >
                                 <div class="help-block with-errors"></div>
                                 <p class="center-align">
                                     <small><b>Ejemplo: correo@correo.com</b></small>
                                  </p>
                            </div>        
                        </div>
                        <div class="form-group row">
                              <div class="col-md-4">
                                   <label for="empadronador">Empadronador</label>
                                  <select id="empadronador" name="empadronador" class="form-control" >
                                       <?php echo (!empty($data->empadronador) && $data->empadronador > '') ? "<option value='".$data->empadronador."' selected>".$data->empadronador."</option>" : '<option selected hidden>Seleccione</option>'; ?> 
                                       <?php 
                                         foreach ($empadronador as $key => $row) { ?>
                                          <option value="<?php echo  $row->nombre." ".$row->apellido ?>"><?php echo  $row->nombre." ".$row->apellido ?></option> 
                                        <?php  } ?>
                                      </select> 
                                  <div class="help-block with-errors"></div>
                              </div> 
                               <div class="col-md-4">
                                    <label for="empadronador">TR</label>
                                     <input type="text" class="form-control" name="tr" id="tr" placeholder=""  value="<?php echo (!empty($data->tr) && $data->tr > '') ? $data->tr : ''; ?>">
                                   <div class="help-block with-errors"></div>
                                 </div>

                         </div>

                    </div>
                </div>
            </div>
        </div>
                    <!-- End SmartWizard Content -->

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

