<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                        <a>Home</a>
                        <a>Censo</a>
                        Corregir
                   </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small><i class="fa fa-list-alt"></i> CENSO DE ACTIVIDADES ECONÓMICAS 2019</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                    <!--   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li> -->
                     
                      <?php if($rol == "Administrador" && $data->estatus <>  "Corregida" or $rol == "Supervisor" && $data->estatus <>  "Corregida"){ ?>
                        <li>
                          <a class="btn-aprobar btn btn-xs btn-default btn-action sw-btn-next" name="Activar" data-toggle="tooltip" data-placement="right" id="<?php echo  $data->idcenso?>">Aprobar</i></a> 
                        </li>

                      
                      <li>
                        <a href="<?php echo get_site_url("censo/censo/editar/".$data->idcenso)?>" class="btn btn-primary sw-btn-next" type="submit">
                                  Editar
                                </a> 
                      </li>
                      <li><a href="<?php echo get_site_url("/censo/censo/home")?>" class="btn btn-primary sw-btn-next" type="submit">
                                  Volver al listado
                                </a>  
                      </li>
                      <?php }?>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  	<div class="row">
                  		<!-- NEW COL START -->
                  		<article class="col-sm-12 col-md-12 col-lg-12">
                        <fieldset>
                          <div class="row">
                          
                          <div class="x_title pull-right">
                            <div class="row">
                              <div class="tile-stats">
                              <div class="col-md-9 col-sm-9 col-xs-6 tile pull-right">
                                <span>Nro. Control</span>
                                <h2> 000<?php echo $data->idcenso?> <i class="fa fa-check-square-o"></i></h2>
                              </div>
                            </div>
                            </div>
                            <div style="clear: both;"></div>
                          </div>
                          </div>
                        </fieldset>
								<fieldset>
									<h4><i class="fa fa-user"></i> Identificación del contribuyente</h4> <hr>
									 <div class="form-group row">
                             <div class="col-md-6">
                                 <label for="razon_social">Razón Social:</label>
                                  <span class="view-text"><?php echo $data->razon_social?></span> 
                              </div>  

                              <div class="col-md-6">
                                   <label for="denominacion_comercial">Denominación Comercial:</label>
                                   <span class="view-text"><?php echo $data->denominacion_comercial?></span> 
                              </div>  
                         </div>     
    

                         <div class="form-group row">
                            <div class="col-md-12">
                                <label for="rif">Rif:</label>
                                <span class="view-text"><?php echo $data->rif?></span>
                            </div>  
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="rif">Local:</label>
                                <span class="view-text"><?php echo $data->local?></span>
                            </div>  
                            <div class="col-md-4">
                                <label for="rif">Medidas:</label>
                                <span class="view-text"><?php echo $data->medidas?></span>
                            </div> 
                            <div class="col-md-4">
                                <label for="rif">Uso:</label>
                                <span class="view-text"><?php echo $data->uso?></span>
                            </div>
                        </div> 

                        
                        <div class="form-group row">
                            <div class="col-md-6">
                                   <label for="avenida_calle">Avenida / Calle:</label>
                                   <span class="view-text"><?php echo $data->avenida_calle?></span> 

                              </div> 
    
                             <div class="col-md-6">
                                 <label for="punto_referencia">Punto de referencia:</label>
                                 <span class="view-text"><?php echo $data->punto_referencia?></span>
                            </div>    
                        </div>
                        <div class="form-group row">
                                <div class="col-md-6">
                                   <label for="edif_quinta">Edif. / Quinta / Casa:</label>
                                    <span class="view-text"><?php echo $data->edif_quinta?></span>
                              </div> 
                              <div class="col-md-6">
                                   <label for="piso">Piso:</label>
                                   <span class="view-text"><?php echo $data->piso?></span>
                              </div> 
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                   <label for="telefono_1">Teléfono 1:</label>
                                   <span class="view-text"><?php echo $data->cod_telefono_1."-".$data->telefono_1?></span>
                              </div> 
                              <div class="col-md-4">
                                   <label for="telefono_2">Redes Sociales:</label>
                                   <span class="view-text"><?php echo $data->rrss?></span>
                              </div> 
                              <div class="col-md-4">
                                   <label for="e_mail">Num.Cta.contrato Electricidad:</label>
                                   <span class="view-text"><?php echo $data->contrato_electricidad?></span>
                              </div> 
                        </div>
                         <div class="form-group row">
                            <div class="col-md-6">
                                   <label for="telefono_1">Correo Electronico:</label>
                                   <span class="view-text"><?php echo $data->e_mail?></span>
                              </div> 
                              <div class="col-md-6">
                                   <label for="telefono_2">Tipo de Negocio:</label>
                                   <span class="view-text"><?php echo $data->tipo_negocio?></span>
                              </div> 
                        </div>

                        <br><h4><i class="fa fa-institution"></i> Actividades Económicas</h4> <hr>
                           
                            
                         <div class="form-group row">
                             <div class="col-md-4">
                                 <label for="licencia_AE">Número de cuenta renta:</label>
                                 <span class="view-text"><?php echo $data->num_cuenta_renta?></span>
                            </div>
                             <div class="col-md-3">
                                <label for="anio_licencia">Licencia de Actividades Económicas:</label>     
                                <span class="view-text"><?php echo $data->lic_a_e?></span>
                            </div>
                             <div class="col-md-2">
                                <label for="exhibida">Exibida:</label>
                                <span class="view-text"><?php echo $data->exhibida?></span>
                            </div>
                             <div class="col-md-2">
                                <label for="exhibida"><br></label>
                                <span class="view-text"><?php echo $data->anio_licencia?></span>
                            </div>     
                        </div>

                         <div class="form-group row">
                             <div class="col-md-12">
                                 <label for="licencia_AE">Liciencia de A.E. Nro:</label>
                                 <span class="view-text"><?php echo $data->licencia_AE?></span>
                            </div>    
                        </div>
                          <div class="form-group row">
                             <div class="col-md-4">
                                 <label for="licencia_AE">¿Presentó Declaración de A.E?:</label>
                                 <span class="view-text"><?php echo $data->declaracion_AE?></span>
                            </div>    
                             <div class="col-md-4">
                                 <label for="licencia_AE">Mes:</label>
                                 <span class="view-text"><?php echo $data->mes_declaracion_AE?></span>
                            </div>     
                             <div class="col-md-4">
                                 <label for="licencia_AE">Año:</label>
                                 <span class="view-text"><?php echo $data->ano_declaracion_AE?></span>
                            </div>    
                        </div>
                           <div class="form-group row">
                             <div class="col-md-8">
                                 <label for="nun_cuenta">¿Exhibe Declaración de A.E?:</label>
                                 <span class="view-text"><?php echo $data->exhibe_AE?></span>
                            </div>  
                        </div>
                      	<br><h4><i class="fa fa-building"></i> Inmuebles urbanos</h4> <hr>

                      	<div class="form-group row">
                             <div class="col-md-6">
                                 <label for="num_identificacion">¿Presentó Solvencia de Inmueble?:</label>
                                  <span class="view-text"><?php echo $data->solvencia_inmueble?></span>
                            </div> 
                            <div class="col-md-6">
                                 <label for="numero_catastral">Año:</label>
                                  <span class="view-text"><?php echo $data->ano_solvencia?></span>
                            </div>   
                            <div class="col-md-6">
                                 <label for="numero_catastral">Inmueble:</label>
                                  <span class="view-text"><?php echo $data->inmueble?></span>
                            </div>   
                        </div>
                        <div class="form-group row">
                              <div class="col-md-6">
                                    <label for="name">Datos:</label>
                                    <span class="view-text"><?php echo $data->datos?></span>
                              </div> 
                             <div class="col-md-6">
                                <label for="ano_solvencia"> Número Catastro</label>
                                <span class="view-text"><?php echo $data->numero_catastral?></span>
                            </div>   
                        </div>
                        <br><h4><i class="fa fa-truck"></i> Vehículos</h4> <hr>

                        <div class="form-group row">
                             <div class="col-md-3">
                                 <label for="num_identificacion">¿Posee vehículos?:</label>
                                  <span class="view-text"><?php echo $data->vehiculos?></span>
                            </div> 
                            <div class="col-md-3">
                                 <label for="numero_catastral">placa:</label>
                                  <span class="view-text"><?php echo $data->placa?></span>
                            </div>   
                              <div class="col-md-3">
                                    <label for="name">Propietario:</label>
                                    <span class="view-text"><?php echo $data->propietario?></span>
                              </div>   
                              <div class="col-md-3">
                                    <label for="name">Serial:</label>
                                    <span class="view-text"><?php echo $data->serial?></span>
                              </div> 
                        </div>
                    </div>
                        <br><h4><i class="fa fa-credit-card"></i> Licencia para el expendio de licores</h4> <hr>

                        <div class="form-group row">
                              <div class="col-md-6">
	                                <label for="licencia_licores">¿Presentó Licencia de Licores?:</label>
	                                 <span class="view-text"><?php echo $data->licencia_licores?></span>
                              </div> 
                             <div class="col-md-2">
                                <label for="ano_licencia_lic">Año</label>
                                <span class="view-text"><?php echo $data->ano_licencia_lic?></span>
                            </div>
                            <div class="col-md-4">
                               <label for="tipo_expendio">Tipo de Expendio:</label>
                               <span class="view-text"><?php echo $data->tipo_expendio?></span>
                            </div>  
                         </div>
                          <div class="form-group row">
                            <div class="col-md-6">
                                 <label for="nro_licencia">Nro. Licencia:</label>
                                 <span class="view-text"><?php echo $data->nro_licencia?></span>
                            </div>
                            <div class="col-md-6">
                                 <label for="nro_licencia">Exhibida:</label>
                                 <span class="view-text"><?php echo $data->licencia_exhibida?></span>
                            </div>
                             
                          </div>

                           <br><h4><i class="fa fa-puzzle-piece"></i>  Apuestas lícitas</h4> <hr>

                          <div class="form-group row">
                              <div class="col-md-6">
                                    <label for="apuestas_licitas">Permiso de Apuestas Lícitas:</label>
                                    <span class="view-text"><?php echo $data->apuestas_licitas?></span>
                              </div> 
                            <div class="col-md-6">
                                 <label for="num_cuenta_apuestas">Maquina traganiqueles:</label>
                                 <span class="view-text"><?php echo $data->maquina_traganiquel?></span>
                            </div>
                         </div>
                          <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="cantidad_maquinas">Cantidad de Máquinas:</label>
                                 <span class="view-text"><?php echo $data->cantidad_maquinas?></span>
                              </div>
                          </div>


                          <br><h4><i class="fa fa-desktop"></i> Publicidad comercial</h4> <hr>

                           <div class="form-group row">
                              <div class="col-md-6">
                                    <label for="aviso_publicitario">¿Permiso de publicidad?:</label>
                                    <span class="view-text"><?php echo $data->aviso_publicitario?></span>                      
                              </div> 
                            <div class="col-md-6">
                                 <label for="num_cuenta_publicidad">Cantidad de Avisos:</label>
                                 <span class="view-text"><?php echo $data->cantidad_avisos?></span> 
                            </div>
                         </div>
                          <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="cantidad_avisos">Tipo de Avisos:</label>
                                 <span class="view-text"><?php echo $data->tipo_aviso?></span> 
                            </div>
                              <div class="col-md-4">
	                                <label for="iluminado">Cantidad de Caras:</label>
	                                <span class="view-text"><?php echo $data->cantidad_caras?></span>
                              </div>
                              </div> 
                              <div class="form-group row">
                                  <div class="col-md-12">
                                      <label for="publicidad_licores">Actividades Económicas Autorizadas:</label>
                                      <span class="view-text"><?php echo $data->activ_eco_autorizadas?></span>
                                  </div>
                              </div>
                              <div class="row form-group">
                                  <div class="col-md-12">
                                      <label for="publicidad_otro_idioma">Actividades Económicas Observadas:</label>
                                      <span class="view-text"><?php echo $data->activ_eco_observadas?></span>
                                  </div>  
                              </div>
                                 <div class="row form-group">
                                  <div class="col-md-12">
                                      <label for="publicidad_otro_idioma">Observaciones:</label>
                                      <span class="view-text"><?php echo $data->observaciones?></span>
                                  </div>  
                              </div>

                                 <br><h4><i class="fa fa-child"></i> Datos personales</h4> <hr>

                                  <legend><i class="fa fa-user"></i> Contribuyente</legend>
                        <div class="form-group row">
                             <div class="col-md-6">
                                 <label for="administrador">Contribuyente:</label>
                                 <span class="view-text"><?php echo $data->contribuyente?></span>
                            </div>
                             <div class="col-md-6">
                                 <label for="cedula_administrador">Cédula Nro.:</label>
                                 <span class="view-text"><?php echo $data->cedula_contribuyente?></span>
                            </div>       
                        </div>
                        <div class="form-group row">
                             <div class="col-md-4">
                                 <label for="telf_fijo_administrador">Teléfono Fijo:</label>
                                 <span class="view-text"><?php echo $data->cod_telf_fijo_contribuyente."-".$data->telf_fijo_contribuyente?></span>
                            </div>
                             <div class="col-md-4">
                                 <label for="telf_movil_administrador">Teléfono Móvil:</label>
                                 <span class="view-text"><?php echo $data->cod_telf_movil_contribuyente."-".$data->telf_movil_contribuyente?></span>
                            </div>
                             <div class="col-md-4">
                                 <label for="email_reprensentante_administrador">E-mail:</label>
                                   <span class="view-text"><?php echo $data->email_contribuyente?></span>
                            </div>        
                        </div>
                        <div class="form-group row">
                             <div class="col-md-6">
                                 <label for="reprensentante_legal">Representante Legal:</label>
                                 <span class="view-text"><?php echo $data->reprensentante_legal?></span>
                            </div>
                             <div class="col-md-6">
                                 <label for="cedula_representante">Cédula Nro.:</label>
                                 <span class="view-text"><?php echo $data->cedula_representante?></span>
                            </div>       
                        </div>
                        <div class="form-group row">
                             <div class="col-md-4">
                                 <label for="telf_fijo_representante">Teléfono Fijo:</label>
                                 <span class="view-text"><?php echo $data->cod_telf_fijo_representante."-".$data->telf_fijo_representante?></span>
                            </div>
                             <div class="col-md-4">
                                 <label for="telf_movil_representante">Teléfono Móvil:</label>
                                 <span class="view-text"><?php echo $data->cod_telf_movil_representante."-".$data->telf_movil_representante?></span>
                            </div>
                             <div class="col-md-4">
                                 <label for="email_reprensentante_legal">E-mail:</label>
                                 <span class="view-text"><?php echo $data->email_reprensentante_legal?></span>
                            </div>        
                        </div>
                       
                        <div class="form-group row">
                             <div class="col-md-6">
                                 <label for="atendidopor">Atendido por:</label>
                                 <span class="view-text"><?php echo $data->atendidopor?></span>
                            </div>
                             <div class="col-md-6">
                                 <label for="cedula_atendidopor">Cédula Nro:</label>
                                 <span class="view-text"><?php echo $data->cedula_atendidopor?></span>
                            </div>       
                        </div>
                        <div class="form-group row">
                             <div class="col-md-4">
                                 <label for="telf_fijo_atendidopor">Teléfono Fijo:</label>
                                  <span class="view-text"><?php echo $data->cod_telf_fijo_atendidopor."-".$data->telf_fijo_atendidopor?></span>
                            </div>
                             <div class="col-md-4">
                                 <label for="telf_movil_atendidopor">Teléfono Móvil:</label>
                                 <span class="view-text"><?php echo $data->cod_telf_movil_atendidopor."-".$data->telf_movil_atendidopor?></span>
                            </div>
                             <div class="col-md-4">
                                 <label for="email_atendidopor">E-mail:</label>
                                  <span class="view-text"><?php echo $data->email_atendidopor?></span>
                            </div>        
                        </div>
 
                       <br><h4><i class="fa fa-check"></i> Validación</h4> <hr>
                         
                        <div class="form-group row">
                              <div class="col-md-4">
                                   <label for="empadronador">Empadronador</label>
                                   <span class="view-text"><?php echo $data->empadronador?></span>
                              </div> 

                              <div class="col-md-4">
                                   <label for="empadronador">TR</label>
                                   <span class="view-text"><?php echo $data->tr?></span>
                              </div> 
                         </div>

                    </div>
                </div>

								</fieldset>
                  		</article>
                  	</div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

